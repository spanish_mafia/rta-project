#pragma once
#include "Bone.h"
#include "Key.h"

class Animation
{
public:
	Animation();

	float GetDuration() { return duration; }
	int GetNumKeyFrames() { return numberOfKeyFrames; }
	std::vector<Key> GetKeyFrames() { return keyframes; }

	void SetDuration(float _duration) { duration = _duration; }

	void AddKeyFrame(Key frame);
	

private:
	float duration;
	int numberOfKeyFrames;
	std::vector<Key> keyframes;

	

};