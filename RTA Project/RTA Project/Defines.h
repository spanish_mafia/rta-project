#pragma once
#include <assert.h>
#include <iostream>
#include <thread>
#include <vector>

#define BACKBUFFER_WIDTH	1024
#define BACKBUFFER_HEIGHT	768
#define PI 3.14159268f
#define zNear 0.1f
#define zFar 100.0f
#define FOV 45.0f
#define SAFE_RELEASE(p) {if(p){p->Release(); p = nullptr;}}
// anytime you create something surround with HR(), will break if not working
#define HR(hr){assert(hr == S_OK);}

#define MULTISAMPLE_COUNT 1
#define CAMERA_MOVEMENT_SPEED 20.0f
#define CAMERA_ROTATIONAL_SPEED 2.0f
#define SHADOW_MAP_SIZE 256
#define SHADOW_MAPPING 1