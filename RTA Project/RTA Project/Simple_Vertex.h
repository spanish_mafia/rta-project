#pragma once
#include <vector>
struct BlendingPair
{
	int index = 0;
	float weight= 0;
};
struct SIMPLE_VERTEX
{
	float position[4];
	float uvw[3];
	float normal[4];
	float color[4];
	float tangent[4];
	unsigned int index[4];
	float weight[4];
	int controlPointIndex;
	std::vector<BlendingPair> pairs;

	SIMPLE_VERTEX(float* xyzw, float* rgba) {
		for (int i = 0; i < 4; i++)
		{
			position[i] = xyzw[i];
			color[i] = rgba[i];
		}
	}
	SIMPLE_VERTEX(float x = 0.0f, float y = 0.0f, float z = 0.0f, float w = 1.0f,
		float u = 0.0f, float v = 0.0f, float uvW = 0.0f,
		float normalx = 0.0f, float normaly = 0.0f, float normalz = 0.0f, float normalw = 0.0f,
		float colorR = 0.0f, float colorG = 0.0f, float colorB = 0.0f, float colorA = 1.0f,
		float tangentx = 0.0f, float tangenty = 0.0f, float tangentz = 0.0f, float tangentw = 0.0f)
	{
		position[0] = x;
		position[1] = y;
		position[2] = z;
		position[3] = w;
		uvw[0] = u;
		uvw[1] = v;
		uvw[2] = uvW;
		normal[0] = normalx;
		normal[1] = normaly;
		normal[2] = normalz;
		normal[3] = normalw;
		color[0] = colorR;
		color[1] = colorG;
		color[2] = colorB;
		color[3] = colorA;
	}
};