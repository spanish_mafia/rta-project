File Structure for names.txt

Model:

ModelIdentifier - 0
FileName
16 float matrix each separated by spaces
texture file path
Normal Map file path

Point Light
lightIdentifier - 1
Point Light Identifier -0 for point,1 for directional, 2 for spot 
4 float array for color separated by spaces
//if point light
4 float array for position separated by spaces
//if directional light
4 float array for direction separated by spaces
//if spot light
4 float array for position separated by spaces
4 float array for direction separated by spaces
float for cone ratio
float for inner cone ratio
float for light radius
float for outer cone ratio