#pragma pack_matrix(row_major)
// #pragma above is only for hlsl, for matrix multiplication
struct INPUT_VERTEX
{
	float4 coordinate : POSITION;
	float3 uv : UVW;
	float4 normal : NORMAL;
	float4 color : COLOR;
	float4 tangent : TANGENT;
	uint4 bones : BONES;
	float4 weights : WEIGHTS;
};

struct OUTPUT_VERTEX
{
	float4 pos : SV_POSITION;
	float2 outdepth : DEPTH;
};

cbuffer SkinnedObject : register(b0)
{
	float4x4 skinnedMatrices[37];
}

cbuffer Scene : register(b1)
{
	float4x4 viewMatrix;
	float4x4 projectionMatrix;
}

OUTPUT_VERTEX main(INPUT_VERTEX input)
{
	OUTPUT_VERTEX output = (OUTPUT_VERTEX)0;

	float4x4 mat = skinnedMatrices[input.bones.x] * input.weights.x;
	mat += skinnedMatrices[input.bones.y] * input.weights.y;
	mat += skinnedMatrices[input.bones.z] * input.weights.z;
	mat += skinnedMatrices[input.bones.w] * input.weights.w;

	output.pos = mul(input.coordinate, mat);
	output.pos = mul(output.pos, viewMatrix);
	output.pos = mul(output.pos, projectionMatrix);
	output.outdepth = float2(output.pos.z, output.pos.w);

	return output;
}