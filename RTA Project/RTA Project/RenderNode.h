
#pragma once
#include "Renderer.h"

class RenderNode;
typedef void(*RenderFunc)(RenderNode& node);

// Base class for the RenderContext, RenderMaterial, and RenderShape classes

class RenderNode
{
protected:
	// Points to the linked list of renderable objects
	RenderNode* next;
public:
	// Constructor
	RenderNode() { next = nullptr; }

	// Function Pointer that should point at the desired render function
	RenderFunc func;

	// Processes the render function that our RenderFunc is pointing at
	void RenderProcess() { func(*this); }
	
	// Accessors && Mutators
	RenderNode* GetNext() { return next; }
	void SetNext(RenderNode* ptr) { next = ptr; }
};