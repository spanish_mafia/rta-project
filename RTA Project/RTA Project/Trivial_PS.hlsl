#pragma pack_matrix(row_major)
texture2D baseTexture : register(t0);
texture2D normalTexture : register(t1);
texture2D shadowMap : register(t2);

SamplerState sState : register(s0);

cbuffer DIRECTIONAL_LIGHT : register(b0)
{
	float4 directionDL;
	float4 colorDL;
};

cbuffer POINT_LIGHT : register(b1)
{
	float4 positionPL;
	float4 colorPL;
	float4 lightRadiusPL;
};

cbuffer SPOT_LIGHT : register(b2)
{
	float4 coneDirectionSL;
	float4 positionSL;
	float4 colorSL;
	float coneRatioSL;
	float lightRadiusSL;
	float innerConeRatioSL;
	float outerConeRatioSL;
};

cbuffer Scene : register(b3)
{
	float4x4 viewMatrix;
	float4x4 projectionMatrix;
}

struct P_IN
{
	float4 posH : SV_POSITION;
	float3 uvw : UVW;
	float4 normal : NORMAL;
	float4 color : COLOR;
	float4 surfacePosition : SURFACE_POSITION;
	float4 tan : TANGENT;
	float4 bit : BITANGENT;
};

float3 CalcBumpNormal(Texture2D normalMap, SamplerState state, float2 uv, float4 tangent, float4 bitangent, float3 normal){

	float3 bumpNormal;

	bumpNormal = normalMap.Sample(state, uv);

	tangent = normalize(tangent);
	bitangent = normalize(bitangent);
	normal = normalize(normal);

	bumpNormal.xyz = 2.0f * bumpNormal.xyz - float3(1.0f, 1.0f, 1.0f);

	float3x3 TBN;
	TBN[0] = tangent.xyz;
	TBN[1] = bitangent.xyz;
	TBN[2] = normal.xyz;

	TBN[0].x = tangent.x;
	TBN[0].y = bitangent.x;
	TBN[0].z = normal.x;

	TBN[1].x = tangent.y;
	TBN[1].y = bitangent.y;
	TBN[1].z = normal.y;

	TBN[2].x = tangent.z;
	TBN[2].y = bitangent.z;
	TBN[2].z = normal.z;


	bumpNormal = normalize(bumpNormal.xyz);

	bumpNormal = mul(TBN, bumpNormal);

	return bumpNormal;
}

float4 main( P_IN input ) : SV_TARGET
{
	float2 uv;
	float4 surfaceColor;
	float3 BumpNormal;

	uv[0] = input.uvw[0];
	uv[1] = 1 - input.uvw[1];

	BumpNormal = CalcBumpNormal(normalTexture, sState, uv, input.tan, input.bit, input.normal);

	surfaceColor = baseTexture.Sample(sState, uv);

	// need to normalize because if on a curved surface magnitude will be greater than 1
	float4 directionDLCopy = directionDL;
		directionDLCopy = normalize(directionDLCopy);
	float4 norm = normalize(input.normal);
		input.normal = normalize(input.normal); 

	surfaceColor.w = 1;
	// ambient light = lightcolor * surface color
	float4 grayscaleColor = { .1f, .1f, .1f, 1.0f };
		float4 ambientColor = grayscaleColor * surfaceColor;

		// formula for directional light
		float lightRatioDL = saturate(dot(-directionDLCopy, BumpNormal));
	float4 resultDL = lightRatioDL * colorDL * surfaceColor;

	resultDL.w = 1.0f;

	// formula for point light
	float4 directionPL = normalize(positionPL - input.surfacePosition);
		float lightRatioPL = saturate(dot(directionPL, BumpNormal));
	float attenuationPL = 1.0f - (saturate(length((positionPL - input.surfacePosition)) / lightRadiusPL.x));
	float4 resultPL = lightRatioPL * colorPL * surfaceColor * attenuationPL;

	resultPL.w = 1.0f;


	// formula for spot light
	float3 lightDirectionSL = normalize(positionSL.xyz - input.surfacePosition.xyz);
		float surfaceRatioSL = saturate(dot(-lightDirectionSL, coneDirectionSL.xyz));
	float spotFactorSL = (surfaceRatioSL > outerConeRatioSL) ? 1 : 0;
	float4 lightRatioSL = saturate(dot(lightDirectionSL, BumpNormal.xyz));

		float attenuationSL2 = 1.0f - (saturate((innerConeRatioSL - surfaceRatioSL) / (innerConeRatioSL - outerConeRatioSL)));
	float4 resultSL = spotFactorSL * lightRatioSL * colorSL * surfaceColor * attenuationSL2;

	resultSL.w = 1.0f;

	// shadow map stuff
	// shadowFactor between 0 - 1; 1 = lit, 0 = not lit
	// ourDepth should equal this fragment's depth from light

	float4 lightSpacePos = input.surfacePosition;
		lightSpacePos = mul(lightSpacePos, viewMatrix);
	lightSpacePos = mul(lightSpacePos, projectionMatrix);

	// float ourDepth = lightSpacePos.z / lightSpacePos.w;
	float ourDepth = lightSpacePos.z / lightSpacePos.w;

	// texCoords = (lightSpacePos.xy / lightSpacePos.w + 1) / 2;
	float2 texCoords = ((lightSpacePos.xy / lightSpacePos.w) + 1) * 0.5f;
		texCoords.y = 1 - texCoords.y;

	// sampleDepth is depth stored in shadow map at this fragment's position
	float sampleDepth = 0.0f;
	if (texCoords.x < 0 || texCoords.x > 1 || texCoords.y < 0 || texCoords.y > 1)
		sampleDepth = 1.0f;
	else
		sampleDepth = shadowMap.Sample(sState, texCoords).x;
	float shadowFactor = (ourDepth <= sampleDepth);

	return saturate(ambientColor + resultDL + resultPL + (shadowFactor * (resultSL)));
}

