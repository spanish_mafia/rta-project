#define ShadowBias 0.0001f

struct PS_INPUT
{
	float4 pos : SV_POSITION;
	float2 outdepth : DEPTH;
};

float4 main(PS_INPUT input) : SV_TARGET
{
	return (input.outdepth.x / input.outdepth.y) + ShadowBias;
}