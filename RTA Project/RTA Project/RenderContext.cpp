#include "RenderContext.h"

RenderContext::RenderContext()
{
	vBuff = nullptr;
	inputLayout = nullptr;
	iBuff = nullptr;
	vertShader = nullptr;
	pixShader = nullptr;
	model = "";
	ClearSet();
}

RenderContext::~RenderContext()
{
}

void RenderContext::Initialize(ID3D11Buffer* vertexBuffer, ID3D11InputLayout* layout, ID3D11Buffer* indexBuffer, ID3D11VertexShader* vShader, ID3D11PixelShader* pShader,std::string _model)
{
	ClearSet();

	if (vertexBuffer != nullptr)
		vBuff = vertexBuffer;
	if (layout != nullptr)
		inputLayout = layout;
	if (indexBuffer != nullptr)
		iBuff = indexBuffer;
	if (vShader != nullptr)
		vertShader = vShader;
	if (pShader != nullptr)
		pixShader = pShader;

	model = _model;

	func = StandardContextRFunc;
}

void RenderContext::StandardContextRFunc(RenderNode& node)
{
	RenderContext& context = (RenderContext&)node;

	Renderer::iDeviceContext->IASetInputLayout(context.GetInputLayout());
	Renderer::iDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	unsigned int offset = 0;
	unsigned int stride = (unsigned int)sizeof(SIMPLE_VERTEX);
	
	Renderer::iDeviceContext->IASetVertexBuffers(0, 1,context.GetVertexBufferPtr(), &stride, &offset);

	SharedRFunc(node);
}

void RenderContext::SharedRFunc(RenderNode& node)
{
	RenderContext& context = (RenderContext&)node;

	Renderer::iDeviceContext->IASetIndexBuffer(context.GetIndexBuffer(), DXGI_FORMAT_R32_UINT, 0);
	
	Renderer::iDeviceContext->VSSetShader(context.GetVShader(), NULL, 0);
	Renderer::iDeviceContext->PSSetShader(context.GetPShader(), NULL, 0);

	Renderer::Render(context.GetSet());

}