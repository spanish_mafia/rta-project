#pragma once
#include "Renderer.h"
#include "FbxLoader.h"

class Bone
{
private:
	int parentIndex;
	std::string name;
	
	std::vector<int> childenIndices;
	XMFLOAT4X4 globalMatrix;
public:
	Bone() {};
	Bone(int _parentIndex,const char* _name);

	
	int GetParentIndex() { return parentIndex; }
	std::vector<int> GetChildenIndicies() { return childenIndices; }

	const char* GetName() { return name.c_str(); }
	void SetChildrenIndicies(std::vector<int> children) { childenIndices = children; }

	XMFLOAT4X4 GetGlobalMatrix() { return globalMatrix; }
	void SetGlobalMatrix(XMFLOAT4X4 gmat) { globalMatrix = gmat; }
};

