#pragma once
#include "RenderNode.h"
#include "RenderSet.h"

class RenderMaterial : public RenderNode
{
protected:
	ID3D11ShaderResourceView* srvs[2];
	int numSRV;

	std::string texName;

	RenderSet rSetPtr;

public:
	RenderMaterial();
	~RenderMaterial();

	void Initialize(ID3D11ShaderResourceView* diffuse,ID3D11ShaderResourceView* normal,std::string _texName);

	static void DiffuseNormalRFunc(RenderNode& node);

	inline int GetNumResources() { return numSRV; }
	inline std::string GetTexName() { return texName; }
	
	inline ID3D11ShaderResourceView** GetResources() { return srvs; }

	//inline void CreateSet() { rSetPtr = new RenderSet; }
	inline RenderSet& GetSet() { return rSetPtr; }
	inline void AddNodeToSet(RenderNode* node) { rSetPtr.AddRenderNode(node); }
	inline void ClearSet() { rSetPtr.ClearRenderSet(); }
};
// doing per group of objects that are using/making that material, not per object
