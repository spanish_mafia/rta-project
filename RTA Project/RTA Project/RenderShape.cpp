#include "RenderShape.h"

RenderShape::RenderShape()
{
	worldMatrix = XMMatrixIdentity();
	numIndices = 0;
}

void RenderShape::Initialize(XMMATRIX* MatrixPtr, int* _numIndex)
{
	if (MatrixPtr != nullptr)
		worldMatrix = *MatrixPtr;


	if (_numIndex != nullptr)
		numIndices = *_numIndex;

	func = IndexedMeshRFunc;
}

void RenderShape::InitializeAnim(std::vector<XMMATRIX>& MatrixArray, int* _numIndex,int arraySize)
{
	if (MatrixArray.size() > 0)
		pose = MatrixArray;

	if (_numIndex != nullptr)
		numIndices = *_numIndex;

	func = IndexedAnimMeshRFunc;
}

void RenderShape::IndexedMeshRFunc(RenderNode& node)
{
	RenderShape& shape = (RenderShape&)node;

	Renderer::SendObjectData(shape.GetWorldMatrix());
	Renderer::iDeviceContext->DrawIndexed(shape.GetNumIndicies(), 0, 0);
}

void RenderShape::IndexedAnimMeshRFunc(RenderNode& node)
{
	RenderShape& shape = (RenderShape&)node;

	Renderer::SendSkinnedObjectData(shape.GetPose());
	Renderer::iDeviceContext->DrawIndexed(shape.GetNumIndicies(), 0, 0);
}
