#pragma once
#include "Animation.h"

class Interpolator
{
public:
	Interpolator();

	void AddTime(float timeToAdd); 
	void SetTime(float _time);
	float GetTime();
	std::vector<XMMATRIX> GetPose();

	Animation GetAnimation() { return *animationPtr; }
	void SetAnimation(Animation* anim) { animationPtr = anim; }

	void Process();

	

private:
	void ProcessKeyFrames(std::vector<Key>& Kframes);
	
	float currentTime;
	Animation* animationPtr;

	std::vector<XMMATRIX> pose;
};
