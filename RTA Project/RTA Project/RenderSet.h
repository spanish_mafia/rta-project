#pragma once
#include "RenderNode.h"
#include "Renderer.h"

class RenderNode;

class RenderSet
{
	friend class Renderer;

protected:
	// Pointers to the front and back of the list
	RenderNode* head;
	RenderNode* tail;

	int size;

public:
	RenderSet();
	~RenderSet() {};

	// Add a RenderNode to the list
	void AddRenderNode(RenderNode* node);
	
	// Returns the head and tail pointers to their default state
	void ClearRenderSet();

	// Checks to see if the node is in the set
	bool IsInSet(RenderNode* node);
	
	// Accessors && Mutators
	RenderNode* GetHead() { return head; }
};