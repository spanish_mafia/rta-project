#pragma pack_matrix(row_major)
// #pragma above is only for hlsl, for matrix multiplication
struct INPUT_VERTEX
{
	float4 coordinate : POSITION;
	float3 uv : UVW;
	float4 normal : NORMAL;
	float4 color : COLOR;
	float4 tangent : TANGENT;
	uint4 bones : BONES;
	float4 weights : WEIGHTS;
};

struct OUTPUT_VERTEX
{
	float4 pos : SV_POSITION;
	float2 outdepth : DEPTH;
};

cbuffer Object : register(b0)
{
	float4x4 worldMatrix;
};

cbuffer Scene : register(b1)
{
	float4x4 viewMatrix;
	float4x4 projectionMatrix;
}

OUTPUT_VERTEX main(INPUT_VERTEX input)
{
	OUTPUT_VERTEX output = (OUTPUT_VERTEX)0;

	output.pos = mul(input.coordinate, worldMatrix);
	output.pos = mul(output.pos, viewMatrix);
	output.pos = mul(output.pos, projectionMatrix);
	output.outdepth = float2(output.pos.z, output.pos.w);

	return output;
}