#include "Animation.h"

Animation::Animation()
{
	numberOfKeyFrames = 0;
	duration = 0;
}

void Animation::AddKeyFrame(Key frame)
{
	keyframes.push_back(frame);
	numberOfKeyFrames++;
}