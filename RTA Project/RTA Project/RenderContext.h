#pragma once
#include "RenderNode.h"
#include "Renderer.h"
#include "RenderSet.h"


class RenderContext : public RenderNode
{
protected:
	ID3D11Buffer* iBuff;
	ID3D11Buffer* vBuff;
	ID3D11VertexShader* vertShader;
	ID3D11PixelShader* pixShader;
	ID3D11InputLayout* inputLayout;

	RenderSet rSetPtr;

	std::string model;

public:
	RenderContext();
	~RenderContext();


	void Initialize(ID3D11Buffer* vertexBuffer,ID3D11InputLayout* layout,ID3D11Buffer* indexBuffer, ID3D11VertexShader* vShader, ID3D11PixelShader* pShader,std::string _model);

	// Accessors && Mutators
	ID3D11Buffer* GetIndexBuffer() { return iBuff; }
	ID3D11Buffer** GetVertexBufferPtr() { return &vBuff; }
	ID3D11VertexShader* GetVShader() { return vertShader; }
	ID3D11PixelShader* GetPShader() { return pixShader; }
	ID3D11InputLayout* GetInputLayout() { return inputLayout; }
	std::string GetModelName() { return model; }

	// Render Set Methods
	//inline void CreateSet(){ rSetPtr = new RenderSet; }
	inline RenderSet& GetSet() { return rSetPtr; }
	inline void AddNodeToSet(RenderNode* node) { rSetPtr.AddRenderNode(node); }
	inline void ClearSet() { rSetPtr.ClearRenderSet(); }

	// Render Functions
	static void StandardContextRFunc(RenderNode& node);
	static void SharedRFunc(RenderNode& node);
};

