#pragma pack_matrix(row_major)
// #pragma above is only for hlsl, for matrix multiplication
struct INPUT_VERTEX
{
	float4 coordinate : POSITION;
	float3 uv : UVW;
	float4 normal : NORMAL;
	float4 color : COLOR;
	float4 tangent : TANGENT;
	uint4 bones : BONES;
	float4 weights : WEIGHTS;
};

struct OUTPUT_VERTEX
{
	float4 projectedCoordinate : SV_POSITION;
	float3 uvw : UVW;
	float4 normal : NORMAL;
	float4 color : COLOR;
	float4 surfacePosition : SURFACE_POSITION;
	float4 tan : TANGENT;
	float4 bit : BITANGENT;
};

cbuffer Object : register(b0)
{
	// this is a built in data type for hlsl, many others as well
	float4x4 worldMatrix;
};

cbuffer Scene : register(b1)
{
	float4x4 viewMatrix;
	float4x4 projectionMatrix;
}


OUTPUT_VERTEX main(INPUT_VERTEX fromVertexBuffer)
{
	OUTPUT_VERTEX sendToRasterizer = (OUTPUT_VERTEX)0;

	float4 localVertex = float4(fromVertexBuffer.coordinate.xyz, 1.0f);
		sendToRasterizer.normal = mul(float4 (fromVertexBuffer.normal.xyz, 0), worldMatrix);
	sendToRasterizer.tan = mul(worldMatrix, float4(fromVertexBuffer.tangent.xyz, 0.0f));
	sendToRasterizer.bit = float4(cross(sendToRasterizer.normal, sendToRasterizer.tan), 0.0f);

	localVertex = mul(localVertex, worldMatrix);
	sendToRasterizer.surfacePosition = localVertex;
	localVertex = mul(localVertex, viewMatrix);
	localVertex = mul(localVertex, projectionMatrix);

	// directX takes care of the perspective divide in the rasterizer
	sendToRasterizer.projectedCoordinate = localVertex;
	sendToRasterizer.uvw = fromVertexBuffer.uv;
	
	sendToRasterizer.color = fromVertexBuffer.color;

	return sendToRasterizer;
}