#include "Camera.h"
#include "Defines.h"


Camera::Camera()
{
	viewMatrix = XMMatrixIdentity();
	currentMousePosition.x = 0;
	currentMousePosition.y = 0;
	previousMousePosition.x = 0;
	previousMousePosition.y = 0;
}

XMMATRIX Camera::GetViewMatrix()const
{
	return XMMatrixInverse(nullptr, viewMatrix);
}

XMMATRIX Camera::GetWorldMatrix()const
{
	return viewMatrix;
}

void Camera::SetWorldMatrix(const XMMATRIX& worldMatrix)
{
	viewMatrix = worldMatrix;
}

void Camera::UpdateCamera(float deltaTime)
{
	GetCursorPos(&currentMousePosition);
	// zoom in, Local Z Translation
	if (GetAsyncKeyState('W'))
	{
		XMMATRIX m = XMMatrixIdentity();
		m.r[3].m128_f32[2] = (CAMERA_MOVEMENT_SPEED * deltaTime);
		viewMatrix = XMMatrixMultiply(m, viewMatrix);
	}
	// zoom out
	else if (GetAsyncKeyState('S'))
	{
		XMMATRIX m = XMMatrixIdentity();
		m.r[3].m128_f32[2] = (-CAMERA_MOVEMENT_SPEED * deltaTime);
		viewMatrix = XMMatrixMultiply(m, viewMatrix);
	}

	//strafe left, Local X Translation
	if (GetAsyncKeyState('A'))
	{
		XMMATRIX m = XMMatrixIdentity();
		m.r[3].m128_f32[0] = (-CAMERA_MOVEMENT_SPEED * deltaTime);
		viewMatrix = XMMatrixMultiply(m, viewMatrix);
	}
	// strafe right
	else if (GetAsyncKeyState('D'))
	{
		XMMATRIX m = XMMatrixIdentity();
		m.r[3].m128_f32[0] = (CAMERA_MOVEMENT_SPEED * deltaTime);
		viewMatrix = XMMatrixMultiply(m, viewMatrix);
	}

	// fly up, Global Y Translation, matrix multiplication order switched
	if (GetAsyncKeyState('Q'))
	{
		XMMATRIX m = XMMatrixIdentity();
		m.r[3].m128_f32[1] = (CAMERA_MOVEMENT_SPEED * deltaTime);
		viewMatrix = XMMatrixMultiply(viewMatrix, m);
	}
	// fly down
	else if (GetAsyncKeyState('E'))
	{
		XMMATRIX m = XMMatrixIdentity();
		m.r[3].m128_f32[1] = (-CAMERA_MOVEMENT_SPEED * deltaTime);
		viewMatrix = XMMatrixMultiply(viewMatrix, m);
	}

	if (GetAsyncKeyState(VK_RBUTTON))
	{
		// find change in mouse position
		float dx = (float)currentMousePosition.x - (float)previousMousePosition.x;
		float dy = (float)currentMousePosition.y - (float)previousMousePosition.y;
		float radX = dx;
		float radY = dy;

		// x rotation effects y axis, vice versa, need to use dy for xRotation, dx for yRotation
		XMMATRIX xRotationalMatrix = XMMatrixRotationX(radY * CAMERA_ROTATIONAL_SPEED * deltaTime);
		XMMATRIX yRotationalMatrix = XMMatrixRotationY(radX * CAMERA_ROTATIONAL_SPEED * deltaTime);

		// to avoid camera rolling, won't notice until you have 2 or more objects
		XMVECTOR v;
		v.m128_f32[0] = viewMatrix.r[3].m128_f32[0];
		v.m128_f32[1] = viewMatrix.r[3].m128_f32[1];
		v.m128_f32[2] = viewMatrix.r[3].m128_f32[2];
		v.m128_f32[3] = viewMatrix.r[3].m128_f32[3];

		viewMatrix.r[3].m128_f32[0] = 0;
		viewMatrix.r[3].m128_f32[1] = 0;
		viewMatrix.r[3].m128_f32[2] = 0;

		// look up and down, Local X Rotation
		viewMatrix = XMMatrixMultiply(xRotationalMatrix, viewMatrix);
		// look left and right, Global Y Rotation
		viewMatrix = XMMatrixMultiply(viewMatrix, yRotationalMatrix);

		viewMatrix.r[3].m128_f32[0] = v.m128_f32[0];
		viewMatrix.r[3].m128_f32[1] = v.m128_f32[1];
		viewMatrix.r[3].m128_f32[2] = v.m128_f32[2];
		viewMatrix.r[3].m128_f32[3] = v.m128_f32[3];
	}
	previousMousePosition = currentMousePosition;
}