#include "RenderSet.h"
#include "RenderNode.h"


RenderSet::RenderSet() 
{
	size = 0;
	ClearRenderSet();
}

void RenderSet::ClearRenderSet() 
{
	head = nullptr;
	tail = nullptr;
}

void RenderSet::AddRenderNode(RenderNode* node)
{
	// Set the new node's next to null
	node->SetNext(nullptr);
	
	// If the list is empty, add the node to the head
	if (head == nullptr)
		head = node;
	else // If the list is not empty, add the node to the end of the list
		tail->SetNext(node);
	
	// Update the tail pointer
	tail = node;

	size++;
}

bool RenderSet::IsInSet(RenderNode* node)
{
	if (head != nullptr)
	{
		RenderNode* current = head;

		while (current != nullptr)
		{
			if (current == node)
				return true;

			current = current->GetNext();
		}
	}

	return false;
}