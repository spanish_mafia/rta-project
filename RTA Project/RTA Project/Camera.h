#pragma once
#include <Windows.h>
#include <DirectXMath.h>

using namespace DirectX;
class Camera
{
private:
	POINT previousMousePosition;
	POINT currentMousePosition;
	XMMATRIX viewMatrix;

public:
	Camera();
	XMMATRIX GetViewMatrix()const;
	XMMATRIX GetWorldMatrix()const;
	void SetWorldMatrix(const XMMATRIX& worldMatrix);
	void UpdateCamera(float deltaTime);
};