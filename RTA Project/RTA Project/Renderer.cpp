#include "Renderer.h"
#include "RenderSet.h"
#include "Defines.h"

ID3D11Device* Renderer::iDevice = nullptr;
ID3D11DeviceContext* Renderer::iDeviceContext = nullptr;
ID3D11RenderTargetView* Renderer::iRenderTargetView = nullptr;
IDXGISwapChain* Renderer::iSwapChain = nullptr;
D3D11_VIEWPORT Renderer::viewport;
ID3D11Texture2D* Renderer::iBackBuffer = nullptr;
ID3D11Texture2D* Renderer::zBuffer = nullptr;
ID3D11DepthStencilView* Renderer::depthStencil = nullptr;
Renderer::Scene Renderer::toShaderScene;
Camera Renderer::viewMatrix;
ID3D11Buffer* Renderer::cBuffer[];
ID3D11SamplerState* Renderer::sampleState = nullptr;
Renderer::DIRECTIONAL_LIGHT Renderer::directionalLightToShader;
ID3D11Buffer* Renderer::iDirectionalLightConstantBuffer = nullptr;
Renderer::POINT_LIGHT Renderer::pointLightToShader;
ID3D11Buffer* Renderer::iPointLightConstantBuffer = nullptr;
Renderer::SPOT_LIGHT Renderer::spotLightToShader;
ID3D11Buffer* Renderer::iSpotLightConstantBuffer = nullptr;
Renderer::Object Renderer::toShaderObj;
ID3D11Texture2D* Renderer::iShadowMapBuffer = nullptr;
ID3D11RenderTargetView* Renderer::iSMRenderTargetView = nullptr;
ID3D11ShaderResourceView* Renderer::iSMShaderResourceView = nullptr;
D3D11_VIEWPORT Renderer::smViewport;
Renderer::Scene Renderer::toShaderLightViewProj;
ID3D11RasterizerState* Renderer::iRasterizerState = nullptr;
ID3D11DepthStencilView* Renderer::iShadowDepthStencil = nullptr;
ID3D11Texture2D* Renderer::iSMDepthBuffer = nullptr;
Renderer::SkinnedObject Renderer::toShaderSkinnedObj;

void Renderer::Initialize(HWND hWnd)
{

	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	ZeroMemory(&swapChainDesc, sizeof(swapChainDesc));
	// not 2, front buffer already exists as the "desktop"
	swapChainDesc.BufferCount = 1;
	// this swapchain is going to directly draw to a backbuffer
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.OutputWindow = hWnd;
	// true means windowed
	swapChainDesc.Windowed = true;
	// window width and size
	swapChainDesc.BufferDesc.Width = BACKBUFFER_WIDTH;
	swapChainDesc.BufferDesc.Height = BACKBUFFER_HEIGHT;
	// standard 60hz refresh rate
	swapChainDesc.BufferDesc.RefreshRate.Numerator = 1;
	swapChainDesc.BufferDesc.RefreshRate.Denominator = 60;
	// rgba format
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapChainDesc.SampleDesc.Quality = 0;
	swapChainDesc.SampleDesc.Count = MULTISAMPLE_COUNT;

	// if you wanted more flags, would use bitwise or "|"
	// if you don't use this flag, when you go to fullscreen mode, will go to highest resolution
	//swapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
	// only want flag below to run in debug mode
	UINT flag = 0;
#if _DEBUG
	flag = D3D11_CREATE_DEVICE_DEBUG;
#endif
	D3D11CreateDeviceAndSwapChain(0, D3D_DRIVER_TYPE_HARDWARE, 0,
		flag, 0, 0, D3D11_SDK_VERSION, &swapChainDesc, &iSwapChain,
		&iDevice, 0, &iDeviceContext);
	

	iSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&iBackBuffer);
	iDevice->CreateRenderTargetView(iBackBuffer, 0, &iRenderTargetView);

	// viewports
	DXGI_SWAP_CHAIN_DESC swapChainDescCopy;
	// want to make local copy and call GetDesc()
	iSwapChain->GetDesc(&swapChainDescCopy);
	viewport.Height = (float)swapChainDescCopy.BufferDesc.Height;
	viewport.Width = (float)swapChainDescCopy.BufferDesc.Width;
	viewport.MinDepth = 0;
	viewport.MaxDepth = 1;
	viewport.TopLeftX = 0;
	viewport.TopLeftY = 0;

	// depth buffer
	D3D11_TEXTURE2D_DESC zDesc;
	ZeroMemory(&zDesc, sizeof(zDesc));

	zDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	zDesc.Format = DXGI_FORMAT_D32_FLOAT;
	zDesc.Height = BACKBUFFER_HEIGHT;
	zDesc.Width = BACKBUFFER_WIDTH;
	zDesc.ArraySize = 1;
	zDesc.SampleDesc.Count = MULTISAMPLE_COUNT;
	zDesc.SampleDesc.Quality = 0;
	zDesc.CPUAccessFlags = 0;
	zDesc.Usage = D3D11_USAGE_DEFAULT;
	zDesc.MipLevels = 1;
	zDesc.MiscFlags = 0;

	iDevice->CreateTexture2D(&zDesc, nullptr, &zBuffer);

	D3D11_DEPTH_STENCIL_VIEW_DESC DSVdesc;
	ZeroMemory(&DSVdesc, sizeof(DSVdesc));

	DSVdesc.Format = DXGI_FORMAT_D32_FLOAT;
	DSVdesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;

	iDevice->CreateDepthStencilView(zBuffer, &DSVdesc, &depthStencil);

	// SCENE
	D3D11_BUFFER_DESC cBuffDesc2;
	ZeroMemory(&cBuffDesc2, sizeof(cBuffDesc2));

	cBuffDesc2.Usage = D3D11_USAGE_DYNAMIC;
	cBuffDesc2.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cBuffDesc2.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cBuffDesc2.ByteWidth = sizeof(Scene);
	cBuffDesc2.MiscFlags = 0;

	XMMATRIX m = XMMatrixIdentity();
	XMMATRIX xRotationalMatrix = XMMatrixRotationY(XMConvertToRadians(179.0f));
	m = XMMatrixMultiply(m, xRotationalMatrix);
	m.r[3].m128_f32[1] = 5.0f;
	m.r[3].m128_f32[2] = 15.0f;
	viewMatrix.SetWorldMatrix(m);
	toShaderScene.viewMatrix = viewMatrix.GetViewMatrix();
	toShaderScene.projMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(FOV), viewport.Width / viewport.Height, zNear, zFar);

	iDevice->CreateBuffer(&cBuffDesc2, NULL, &cBuffer[1]);


	//Sampler State
	D3D11_SAMPLER_DESC samplerDesc;
	ZeroMemory(&samplerDesc, sizeof(samplerDesc));
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	samplerDesc.MinLOD = -D3D11_FLOAT32_MAX;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.BorderColor[0] = 1.0f;
	samplerDesc.BorderColor[1] = 1.0f;
	samplerDesc.BorderColor[2] = 1.0f;
	samplerDesc.BorderColor[3] = 1.0f;

	iDevice->CreateSamplerState(&samplerDesc, &sampleState);

	// lights
	CreateConstantBuffer(directionalLightToShader, &iDirectionalLightConstantBuffer, iDevice);
	directionalLightToShader.color = { 1.0f, 1.0f, 1.0f, 1.0f };
	directionalLightToShader.direction = { -1.0f, -1.0f, -1.0f, 0 };

	CreateConstantBuffer(pointLightToShader, &iPointLightConstantBuffer, iDevice);
	pointLightToShader.color = { 1.0f, 1.0f, 1.0f, 1.0f };
	pointLightToShader.position = { 0, 2.5f, -1.0f, 0 };
	pointLightToShader.lightRadiusPL = { 7.0f, 0, 0, 0 };

	CreateConstantBuffer(spotLightToShader, &iSpotLightConstantBuffer, iDevice);
	spotLightToShader.color = { 1.0f, 1.0f, 1.0f, 1.0f };
	spotLightToShader.position = { 3.0f, 12.0f, 0.0f, 1.0f };
	//spotLightToShader.coneDirection = { 0, -1.0f, 0, 0 };
	spotLightToShader.coneDirection = { -.707f, -.707f, 0.001f, 0 };
	XMVECTOR v = XMLoadFloat4(&spotLightToShader.coneDirection);
	v = XMVector3Normalize(v);
	XMStoreFloat4(&spotLightToShader.coneDirection, v);
	spotLightToShader.coneRatio = 0.0f;
	spotLightToShader.lightRadiusSL = 30.0f;
	spotLightToShader.innerConeRatioSL = .99f;
	// outer cone ratio = spot cutoff
	spotLightToShader.outerConeRatioSL = .88f;

	// shadow map
	smViewport.Height = SHADOW_MAP_SIZE;
	smViewport.Width = SHADOW_MAP_SIZE;
	smViewport.MinDepth = 0;
	smViewport.MaxDepth = 1;
	smViewport.TopLeftX = 0;
	smViewport.TopLeftY = 0;

	D3D11_TEXTURE2D_DESC shadowDesc;
	ZeroMemory(&shadowDesc, sizeof(shadowDesc));
	shadowDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	shadowDesc.Format = DXGI_FORMAT_R32_FLOAT;
	shadowDesc.Height = SHADOW_MAP_SIZE;
	shadowDesc.Width = SHADOW_MAP_SIZE;
	shadowDesc.ArraySize = 1;
	shadowDesc.SampleDesc.Count = 1;
	shadowDesc.SampleDesc.Quality = 0;
	shadowDesc.CPUAccessFlags = 0;
	shadowDesc.Usage = D3D11_USAGE_DEFAULT;
	shadowDesc.MipLevels = 1;
	shadowDesc.MiscFlags = 0;

	toShaderLightViewProj.viewMatrix = XMMatrixIdentity();
	toShaderLightViewProj.projMatrix = XMMatrixIdentity();

	D3D11_BUFFER_DESC cBuffDesc3;
	ZeroMemory(&cBuffDesc3, sizeof(cBuffDesc3));

	cBuffDesc3.Usage = D3D11_USAGE_DYNAMIC;
	cBuffDesc3.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cBuffDesc3.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cBuffDesc3.ByteWidth = sizeof(Scene);
	cBuffDesc3.MiscFlags = 0;

	iDevice->CreateBuffer(&cBuffDesc3, NULL, &cBuffer[2]);

	iDevice->CreateTexture2D(&shadowDesc, nullptr, &iShadowMapBuffer);
	iDevice->CreateRenderTargetView(iShadowMapBuffer, 0, &iSMRenderTargetView);
	iDevice->CreateShaderResourceView(iShadowMapBuffer, nullptr, &iSMShaderResourceView);

	shadowDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	shadowDesc.Format = DXGI_FORMAT_D32_FLOAT;
	iDevice->CreateTexture2D(&shadowDesc, nullptr, &iSMDepthBuffer);

	D3D11_DEPTH_STENCIL_VIEW_DESC DSVdesc1;
	ZeroMemory(&DSVdesc1, sizeof(DSVdesc1));
	DSVdesc1.Format = DXGI_FORMAT_D32_FLOAT;
	DSVdesc1.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;
	iDevice->CreateDepthStencilView(iSMDepthBuffer, &DSVdesc1, &iShadowDepthStencil);

	D3D11_RASTERIZER_DESC rasterDesc;
	ZeroMemory(&rasterDesc, sizeof(rasterDesc));

	rasterDesc.FillMode = D3D11_FILL_SOLID;
	rasterDesc.CullMode = D3D11_CULL_NONE;
	rasterDesc.DepthClipEnable = TRUE;

	iDevice->CreateRasterizerState(&rasterDesc, &iRasterizerState);
	iDeviceContext->RSSetState(iRasterizerState);
}

void Renderer::Render(RenderSet& set)
{
	RenderNode* current = set.GetHead();

	while (current != nullptr)
	{
		current->RenderProcess();
		current = current->GetNext();
	}
}

void Renderer::Shutdown()
{
	SAFE_RELEASE(iDevice);
	SAFE_RELEASE(iDeviceContext);
	SAFE_RELEASE(iRenderTargetView);
	SAFE_RELEASE(iSwapChain);
	SAFE_RELEASE(iBackBuffer);
	SAFE_RELEASE(zBuffer);
	SAFE_RELEASE(depthStencil);
	SAFE_RELEASE(cBuffer[0]);
	SAFE_RELEASE(cBuffer[1]);
	SAFE_RELEASE(cBuffer[2]);
	SAFE_RELEASE(cBuffer[3]);
	SAFE_RELEASE(sampleState);
	SAFE_RELEASE(iDirectionalLightConstantBuffer);
	SAFE_RELEASE(iPointLightConstantBuffer);
	SAFE_RELEASE(iSpotLightConstantBuffer);
	SAFE_RELEASE(iShadowMapBuffer);
	SAFE_RELEASE(iSMRenderTargetView);
	SAFE_RELEASE(iSMShaderResourceView);
	SAFE_RELEASE(iRasterizerState);
	SAFE_RELEASE(iShadowDepthStencil);
	SAFE_RELEASE(iSMDepthBuffer);
}

void Renderer::ClearRenderTargetView(float r, float g, float b, float a)
{
	float color[4] = { r,g,b,a };
	iDeviceContext->ClearRenderTargetView(iRenderTargetView, color);
}

void Renderer::Resize()
{
	if (iSwapChain)
	{
		iDeviceContext->OMSetRenderTargets(0, 0, 0);
		// Release all outstanding references to the swap chain's buffers, 
		// then recreate desc for each released item, then set them
		iRenderTargetView->Release();
		iBackBuffer->Release();
		zBuffer->Release();
		depthStencil->Release();
		iDeviceContext->ClearState();
		iSwapChain->ResizeBuffers(0, 0, 0, DXGI_FORMAT_UNKNOWN, 0);
		iSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&iBackBuffer);

		iDevice->CreateRenderTargetView(iBackBuffer, NULL, &iRenderTargetView);

		DXGI_SWAP_CHAIN_DESC swapChainDescCopy;
		iSwapChain->GetDesc(&swapChainDescCopy);
		viewport.Height = (float)swapChainDescCopy.BufferDesc.Height;
		viewport.Width = (float)swapChainDescCopy.BufferDesc.Width;
		viewport.MinDepth = 0;
		viewport.MaxDepth = 1;
		viewport.TopLeftX = 0;
		viewport.TopLeftY = 0;

		D3D11_TEXTURE2D_DESC zDesc;
		ZeroMemory(&zDesc, sizeof(zDesc));
		zDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		zDesc.Format = DXGI_FORMAT_D32_FLOAT;
		zDesc.Height = swapChainDescCopy.BufferDesc.Height;
		zDesc.Width = swapChainDescCopy.BufferDesc.Width;
		zDesc.ArraySize = 1;
		zDesc.SampleDesc.Count = MULTISAMPLE_COUNT;
		zDesc.SampleDesc.Quality = 0;
		zDesc.CPUAccessFlags = 0;
		zDesc.Usage = D3D11_USAGE_DEFAULT;
		zDesc.MipLevels = 1;
		zDesc.MiscFlags = 0;
		iDevice->CreateTexture2D(&zDesc, nullptr, &zBuffer);
		iDevice->CreateDepthStencilView(zBuffer, nullptr, &depthStencil);

		// need to rebuild projection matrix here, toshaderscene 
		toShaderScene.projMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(FOV), viewport.Width / viewport.Height, zNear, zFar);

		iDeviceContext->RSSetViewports(1, &viewport);
		iDeviceContext->OMSetRenderTargets(1, &iRenderTargetView, depthStencil);
	}
}

void Renderer::CreateTexture(string filename, std::vector<ID3D11ShaderResourceView*>& srv)
{
	ID3D11ShaderResourceView* temp = nullptr;
	wstring name = wstring(filename.begin(), filename.end());
	CreateDDSTextureFromFile(iDevice, name.c_str(), NULL, &temp);
	srv.push_back(temp);
}

void Renderer::CreateBuffer(std::vector<ID3D11Buffer*>& buff, UINT bindFlags, UINT byteWidth, const void* sysMem)
{
	D3D11_BUFFER_DESC BuffDesc;
	ZeroMemory(&BuffDesc, sizeof(BuffDesc));
	
	BuffDesc.Usage = D3D11_USAGE_IMMUTABLE;
	BuffDesc.BindFlags = bindFlags;
	BuffDesc.CPUAccessFlags = NULL;
	BuffDesc.ByteWidth = byteWidth;

	D3D11_SUBRESOURCE_DATA subresource;
	ZeroMemory(&subresource, sizeof(subresource));


	subresource.pSysMem = sysMem;

	ID3D11Buffer* tempbuff = NULL;


	iDevice->CreateBuffer(&BuffDesc, &subresource, &tempbuff);
	buff.push_back(tempbuff);
}

void Renderer::SendObjectData(XMMATRIX& _world)
{
	toShaderObj.worldMatrix = _world;

	D3D11_MAPPED_SUBRESOURCE mSubresource;
	iDeviceContext->Map(cBuffer[0], 0, D3D11_MAP_WRITE_DISCARD, 0, &mSubresource);
	memcpy(mSubresource.pData, &toShaderObj, sizeof(Object));
	iDeviceContext->Unmap(cBuffer[0], 0);

	iDeviceContext->VSSetConstantBuffers(0, 1, &cBuffer[0]); 
}

void Renderer::SendSkinnedObjectData(std::vector<XMMATRIX>& skinMats)
{
	D3D11_MAPPED_SUBRESOURCE mSubresource;
	iDeviceContext->Map(cBuffer[3], 0, D3D11_MAP_WRITE_DISCARD, 0, &mSubresource);
	memcpy(mSubresource.pData, skinMats.data(), sizeof(XMMATRIX) * skinMats.size());
	iDeviceContext->Unmap(cBuffer[3], 0);

	iDeviceContext->VSSetConstantBuffers(0, 1, &cBuffer[3]);
}

void Renderer::UpdateDirectionLight(float deltaTime, DIRECTIONAL_LIGHT &directionalLight)
{
	// change direction up y axis
	if (GetAsyncKeyState('I'))
	{
		directionalLight.direction.y += (CAMERA_ROTATIONAL_SPEED * deltaTime);
		if (directionalLight.direction.y > 1.0f)
			directionalLight.direction.y = 1.0f;
		return;
	}
	// change direction down y axis
	else if (GetAsyncKeyState('K'))
	{
		directionalLight.direction.y += (-CAMERA_ROTATIONAL_SPEED * deltaTime);
		if (directionalLight.direction.y < -1.0f)
			directionalLight.direction.y = -1.0f;
		return;
	}

	// change direction left x axis
	if (GetAsyncKeyState('J'))
	{
		directionalLight.direction.x += (-CAMERA_ROTATIONAL_SPEED * deltaTime);
		if (directionalLight.direction.x < -1.0f)
			directionalLight.direction.x = -1.0f;
		return;
	}
	// change direction right x axis
	else if (GetAsyncKeyState('L'))
	{
		directionalLight.direction.x += (CAMERA_ROTATIONAL_SPEED * deltaTime);
		if (directionalLight.direction.x > 1.0f)
			directionalLight.direction.x = 1.0f;
		return;
	}
}

void Renderer::UpdatePointLight(float deltaTime, POINT_LIGHT &pointLight)
{
	// move up y axis
	if (GetAsyncKeyState('U'))
	{
		XMMATRIX plMatrix = XMMatrixIdentity();
		plMatrix.r[3].m128_f32[0] = pointLight.position.x;
		plMatrix.r[3].m128_f32[1] = pointLight.position.y;
		plMatrix.r[3].m128_f32[2] = pointLight.position.z;
		plMatrix.r[3].m128_f32[3] = pointLight.position.w;
		XMMATRIX m = XMMatrixIdentity();
		m.r[3].m128_f32[1] = (CAMERA_MOVEMENT_SPEED * deltaTime);
		plMatrix = XMMatrixMultiply(m, plMatrix);
		pointLight.position.x = plMatrix.r[3].m128_f32[0];
		pointLight.position.y = plMatrix.r[3].m128_f32[1];
		pointLight.position.z = plMatrix.r[3].m128_f32[2];
		pointLight.position.w = plMatrix.r[3].m128_f32[3];
		return;
	}
	// move down y axis
	else if (GetAsyncKeyState('O'))
	{
		XMMATRIX plMatrix = XMMatrixIdentity();
		plMatrix.r[3].m128_f32[0] = pointLight.position.x;
		plMatrix.r[3].m128_f32[1] = pointLight.position.y;
		plMatrix.r[3].m128_f32[2] = pointLight.position.z;
		plMatrix.r[3].m128_f32[3] = pointLight.position.w;
		XMMATRIX m = XMMatrixIdentity();
		m.r[3].m128_f32[1] = (-CAMERA_MOVEMENT_SPEED * deltaTime);
		plMatrix = XMMatrixMultiply(m, plMatrix);
		pointLight.position.x = plMatrix.r[3].m128_f32[0];
		pointLight.position.y = plMatrix.r[3].m128_f32[1];
		pointLight.position.z = plMatrix.r[3].m128_f32[2];
		pointLight.position.w = plMatrix.r[3].m128_f32[3];
		return;
	}

	// move left x axis
	if (GetAsyncKeyState('L'))
	{
		XMMATRIX plMatrix = XMMatrixIdentity();
		plMatrix.r[3].m128_f32[0] = pointLight.position.x;
		plMatrix.r[3].m128_f32[1] = pointLight.position.y;
		plMatrix.r[3].m128_f32[2] = pointLight.position.z;
		plMatrix.r[3].m128_f32[3] = pointLight.position.w;
		XMMATRIX m = XMMatrixIdentity();
		m.r[3].m128_f32[0] = (-CAMERA_MOVEMENT_SPEED * deltaTime);
		plMatrix = XMMatrixMultiply(m, plMatrix);
		pointLight.position.x = plMatrix.r[3].m128_f32[0];
		pointLight.position.y = plMatrix.r[3].m128_f32[1];
		pointLight.position.z = plMatrix.r[3].m128_f32[2];
		pointLight.position.w = plMatrix.r[3].m128_f32[3];
		return;
	}
	// move right x axis
	else if (GetAsyncKeyState('J'))
	{
		XMMATRIX plMatrix = XMMatrixIdentity();
		plMatrix.r[3].m128_f32[0] = pointLight.position.x;
		plMatrix.r[3].m128_f32[1] = pointLight.position.y;
		plMatrix.r[3].m128_f32[2] = pointLight.position.z;
		plMatrix.r[3].m128_f32[3] = pointLight.position.w;
		XMMATRIX m = XMMatrixIdentity();
		m.r[3].m128_f32[0] = (CAMERA_MOVEMENT_SPEED * deltaTime);
		plMatrix = XMMatrixMultiply(m, plMatrix);
		pointLight.position.x = plMatrix.r[3].m128_f32[0];
		pointLight.position.y = plMatrix.r[3].m128_f32[1];
		pointLight.position.z = plMatrix.r[3].m128_f32[2];
		pointLight.position.w = plMatrix.r[3].m128_f32[3];
		return;
	}

	// move forward on z axis
	if (GetAsyncKeyState('I'))
	{
		XMMATRIX plMatrix = XMMatrixIdentity();
		plMatrix.r[3].m128_f32[0] = pointLight.position.x;
		plMatrix.r[3].m128_f32[1] = pointLight.position.y;
		plMatrix.r[3].m128_f32[2] = pointLight.position.z;
		plMatrix.r[3].m128_f32[3] = pointLight.position.w;
		XMMATRIX m = XMMatrixIdentity();
		m.r[3].m128_f32[2] = (-CAMERA_MOVEMENT_SPEED * deltaTime);
		plMatrix = XMMatrixMultiply(m, plMatrix);
		pointLight.position.x = plMatrix.r[3].m128_f32[0];
		pointLight.position.y = plMatrix.r[3].m128_f32[1];
		pointLight.position.z = plMatrix.r[3].m128_f32[2];
		pointLight.position.w = plMatrix.r[3].m128_f32[3];
		return;
	}
	// move backward on z axis
	else if (GetAsyncKeyState('K'))
	{
		XMMATRIX plMatrix = XMMatrixIdentity();
		plMatrix.r[3].m128_f32[0] = pointLight.position.x;
		plMatrix.r[3].m128_f32[1] = pointLight.position.y;
		plMatrix.r[3].m128_f32[2] = pointLight.position.z;
		plMatrix.r[3].m128_f32[3] = pointLight.position.w;
		XMMATRIX m = XMMatrixIdentity();
		m.r[3].m128_f32[2] = (CAMERA_MOVEMENT_SPEED * deltaTime);
		plMatrix = XMMatrixMultiply(m, plMatrix);
		pointLight.position.x = plMatrix.r[3].m128_f32[0];
		pointLight.position.y = plMatrix.r[3].m128_f32[1];
		pointLight.position.z = plMatrix.r[3].m128_f32[2];
		pointLight.position.w = plMatrix.r[3].m128_f32[3];
		return;
	}
}

void Renderer::UpdateSpotLight(float deltaTime, SPOT_LIGHT &spotLight)
{
	// move up y axis
	if (GetAsyncKeyState('U'))
	{
		XMMATRIX slMatrix = XMMatrixIdentity();
		slMatrix.r[3].m128_f32[0] = spotLight.position.x;
		slMatrix.r[3].m128_f32[1] = spotLight.position.y;
		slMatrix.r[3].m128_f32[2] = spotLight.position.z;
		slMatrix.r[3].m128_f32[3] = spotLight.position.w;
		XMMATRIX m = XMMatrixIdentity();
		m.r[3].m128_f32[1] = (CAMERA_MOVEMENT_SPEED * deltaTime);
		slMatrix = XMMatrixMultiply(m, slMatrix);
		spotLight.position.x = slMatrix.r[3].m128_f32[0];
		spotLight.position.y = slMatrix.r[3].m128_f32[1];
		spotLight.position.z = slMatrix.r[3].m128_f32[2];
		spotLight.position.w = slMatrix.r[3].m128_f32[3];
		return;
	}
	// move down y axis
	else if (GetAsyncKeyState('O'))
	{
		XMMATRIX slMatrix = XMMatrixIdentity();
		slMatrix.r[3].m128_f32[0] = spotLight.position.x;
		slMatrix.r[3].m128_f32[1] = spotLight.position.y;
		slMatrix.r[3].m128_f32[2] = spotLight.position.z;
		slMatrix.r[3].m128_f32[3] = spotLight.position.w;
		XMMATRIX m = XMMatrixIdentity();
		m.r[3].m128_f32[1] = (-CAMERA_MOVEMENT_SPEED * deltaTime);
		slMatrix = XMMatrixMultiply(m, slMatrix);
		spotLight.position.x = slMatrix.r[3].m128_f32[0];
		spotLight.position.y = slMatrix.r[3].m128_f32[1];
		spotLight.position.z = slMatrix.r[3].m128_f32[2];
		spotLight.position.w = slMatrix.r[3].m128_f32[3];
		return;
	}

	// move left x axis
	if (GetAsyncKeyState('L'))
	{
		XMMATRIX slMatrix = XMMatrixIdentity();
		slMatrix.r[3].m128_f32[0] = spotLight.position.x;
		slMatrix.r[3].m128_f32[1] = spotLight.position.y;
		slMatrix.r[3].m128_f32[2] = spotLight.position.z;
		slMatrix.r[3].m128_f32[3] = spotLight.position.w;
		XMMATRIX m = XMMatrixIdentity();
		m.r[3].m128_f32[0] = (-CAMERA_MOVEMENT_SPEED * deltaTime);
		slMatrix = XMMatrixMultiply(m, slMatrix);
		spotLight.position.x = slMatrix.r[3].m128_f32[0];
		spotLight.position.y = slMatrix.r[3].m128_f32[1];
		spotLight.position.z = slMatrix.r[3].m128_f32[2];
		spotLight.position.w = slMatrix.r[3].m128_f32[3];
		return;
	}
	// move right x axis
	else if (GetAsyncKeyState('J'))
	{
		XMMATRIX slMatrix = XMMatrixIdentity();
		slMatrix.r[3].m128_f32[0] = spotLight.position.x;
		slMatrix.r[3].m128_f32[1] = spotLight.position.y;
		slMatrix.r[3].m128_f32[2] = spotLight.position.z;
		slMatrix.r[3].m128_f32[3] = spotLight.position.w;
		XMMATRIX m = XMMatrixIdentity();
		m.r[3].m128_f32[0] = (CAMERA_MOVEMENT_SPEED * deltaTime);
		slMatrix = XMMatrixMultiply(m, slMatrix);
		spotLight.position.x = slMatrix.r[3].m128_f32[0];
		spotLight.position.y = slMatrix.r[3].m128_f32[1];
		spotLight.position.z = slMatrix.r[3].m128_f32[2];
		spotLight.position.w = slMatrix.r[3].m128_f32[3];
		return;
	}

	// move forward on z axis
	if (GetAsyncKeyState('I'))
	{
		XMMATRIX slMatrix = XMMatrixIdentity();
		slMatrix.r[3].m128_f32[0] = spotLight.position.x;
		slMatrix.r[3].m128_f32[1] = spotLight.position.y;
		slMatrix.r[3].m128_f32[2] = spotLight.position.z;
		slMatrix.r[3].m128_f32[3] = spotLight.position.w;
		XMMATRIX m = XMMatrixIdentity();
		m.r[3].m128_f32[2] = (-CAMERA_MOVEMENT_SPEED * deltaTime);
		slMatrix = XMMatrixMultiply(m, slMatrix);
		spotLight.position.x = slMatrix.r[3].m128_f32[0];
		spotLight.position.y = slMatrix.r[3].m128_f32[1];
		spotLight.position.z = slMatrix.r[3].m128_f32[2];
		spotLight.position.w = slMatrix.r[3].m128_f32[3];
		return;
	}
	// move backward on z axis
	else if (GetAsyncKeyState('K'))
	{
		XMMATRIX slMatrix = XMMatrixIdentity();
		slMatrix.r[3].m128_f32[0] = spotLight.position.x;
		slMatrix.r[3].m128_f32[1] = spotLight.position.y;
		slMatrix.r[3].m128_f32[2] = spotLight.position.z;
		slMatrix.r[3].m128_f32[3] = spotLight.position.w;
		XMMATRIX m = XMMatrixIdentity();
		m.r[3].m128_f32[2] = (CAMERA_MOVEMENT_SPEED * deltaTime);
		slMatrix = XMMatrixMultiply(m, slMatrix);
		spotLight.position.x = slMatrix.r[3].m128_f32[0];
		spotLight.position.y = slMatrix.r[3].m128_f32[1];
		spotLight.position.z = slMatrix.r[3].m128_f32[2];
		spotLight.position.w = slMatrix.r[3].m128_f32[3];
		return;
	}

	// change direction backward z axis
	if (GetAsyncKeyState(VK_UP))
	{
		spotLight.coneDirection.z += (CAMERA_ROTATIONAL_SPEED * deltaTime);
		if (spotLight.coneDirection.z > 1.0f)
			spotLight.coneDirection.z = 1.0f;
		return;
	}
	// change direction forward z axis
	else if (GetAsyncKeyState(VK_DOWN))
	{
		spotLight.coneDirection.z += (-CAMERA_ROTATIONAL_SPEED * deltaTime);
		if (spotLight.coneDirection.z < -1.0f)
			spotLight.coneDirection.z = -1.0f;
		return;
	}

	// change direction left x axis
	if (GetAsyncKeyState(VK_LEFT))
	{
		spotLight.coneDirection.x += (-CAMERA_ROTATIONAL_SPEED * deltaTime);
		if (spotLight.coneDirection.x < -1.0f)
			spotLight.coneDirection.x = -1.0f;
		return;
	}
	// change direction right x axis
	else if (GetAsyncKeyState(VK_RIGHT))
	{
		spotLight.coneDirection.x += (CAMERA_ROTATIONAL_SPEED * deltaTime);
		if (spotLight.coneDirection.x > 1.0f)
			spotLight.coneDirection.x = 1.0f;
		return;
	}
}