#include "DemoApp.h"
#include <iostream>
#include <thread>
#include <vector>
#include "SimpleVertex_VS.csh"
#include "Trivial_PS.csh"
#include "ShadowMapping_VS.csh"
#include "ShadowMapping_PS.csh"
#include "SimpleAnimation_VS.csh"
#include "NonAnimatedShadowMapping_VS.csh"


using namespace std;
DEMO_APP* DEMO_APP::pDemoApp = nullptr;

DEMO_APP::DEMO_APP(HINSTANCE hinst, WNDPROC proc)
{
	// ****************** BEGIN WARNING ***********************// 
	// WINDOWS CODE, I DON'T TEACH THIS YOU MUST KNOW IT ALREADY! 
	application = hinst;
	appWndProc = proc;

	WNDCLASSEX  wndClass;
	ZeroMemory(&wndClass, sizeof(wndClass));
	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.lpfnWndProc = appWndProc;
	wndClass.lpszClassName = L"DirectXApplication";
	wndClass.hInstance = application;
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)(COLOR_WINDOWFRAME);
	//wndClass.hIcon			= LoadIcon(GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_FSICON));
	RegisterClassEx(&wndClass);

	RECT window_size = { 0, 0, BACKBUFFER_WIDTH, BACKBUFFER_HEIGHT };
	AdjustWindowRect(&window_size, WS_OVERLAPPEDWINDOW, false);

	window = CreateWindow(L"DirectXApplication", L"Spanish_Mafia++ RTA Project", WS_OVERLAPPEDWINDOW /*& ~(WS_THICKFRAME|WS_MAXIMIZEBOX)*/,
		CW_USEDEFAULT, CW_USEDEFAULT, window_size.right - window_size.left, window_size.bottom - window_size.top,
		NULL, NULL, application, this);

	ShowWindow(window, SW_SHOW);
	//********************* END WARNING ************************//

	renderer.Initialize(window);

	// Load FBX Here
	fbx.setNamesFile("names.txt");
	fbx.BeginLoading();

	for each (Mesh mesh in fbx.meshes)
	{
		if (mesh.skeleton.joints.size() > 0)
		{
			// multiply by bind pose
			for each (SIMPLE_VERTEX vert in mesh.verts)
			{
				if (vert.pairs.size() > 0)
				{
					XMFLOAT4 tempVert = XMFLOAT4(vert.position);
					XMVECTOR tempVec = XMLoadFloat4(&tempVert);
				
					XMMATRIX cInverse = XMMatrixIdentity();
					
					for (unsigned int i = 0; i < vert.pairs.size(); i++)
					{
							XMFLOAT4X4 inverse;
							inverse._11 = (float)mesh.skeleton.joints[vert.index[i]].globalBindInverseMatrix.mData[0][0] * vert.weight[i];
							inverse._12 = (float)mesh.skeleton.joints[vert.index[i]].globalBindInverseMatrix.mData[0][1] * vert.weight[i];
							inverse._13 = (float)mesh.skeleton.joints[vert.index[i]].globalBindInverseMatrix.mData[0][2] * vert.weight[i];
							inverse._14 = (float)mesh.skeleton.joints[vert.index[i]].globalBindInverseMatrix.mData[0][3] * vert.weight[i];
																						  										 	 
							inverse._21 = (float)mesh.skeleton.joints[vert.index[i]].globalBindInverseMatrix.mData[1][0] * vert.weight[i];
							inverse._22 = (float)mesh.skeleton.joints[vert.index[i]].globalBindInverseMatrix.mData[1][1] * vert.weight[i];
							inverse._23 = (float)mesh.skeleton.joints[vert.index[i]].globalBindInverseMatrix.mData[1][2] * vert.weight[i];
							inverse._24 = (float)mesh.skeleton.joints[vert.index[i]].globalBindInverseMatrix.mData[1][3] * vert.weight[i];
																						  										 	 			
							inverse._31 = (float)mesh.skeleton.joints[vert.index[i]].globalBindInverseMatrix.mData[2][0] * vert.weight[i];
							inverse._32 = (float)mesh.skeleton.joints[vert.index[i]].globalBindInverseMatrix.mData[2][1] * vert.weight[i];
							inverse._33 = (float)mesh.skeleton.joints[vert.index[i]].globalBindInverseMatrix.mData[2][2] * vert.weight[i];
							inverse._34 = (float)mesh.skeleton.joints[vert.index[i]].globalBindInverseMatrix.mData[2][3] * vert.weight[i];
																				  											   			
							inverse._41 = (float)mesh.skeleton.joints[vert.index[i]].globalBindInverseMatrix.mData[3][0] * vert.weight[i];
							inverse._42 = (float)mesh.skeleton.joints[vert.index[i]].globalBindInverseMatrix.mData[3][1] * vert.weight[i];
							inverse._43 = (float)mesh.skeleton.joints[vert.index[i]].globalBindInverseMatrix.mData[3][2] * vert.weight[i];
							inverse._44 = (float)mesh.skeleton.joints[vert.index[i]].globalBindInverseMatrix.mData[3][3] * vert.weight[i];
				
						cInverse += XMMatrixInverse(NULL, XMLoadFloat4x4(&inverse));
					}
				
					tempVec = XMVector4Transform(tempVec, cInverse);
					XMStoreFloat4(&tempVert, tempVec);
				
					vert.position[0] = tempVert.x;
					vert.position[1] = tempVert.y;
					vert.position[2] = tempVert.z;
					vert.position[3] = tempVert.w;
				}
				
			}

			unsigned int numFrames = mesh.skeleton.joints[0].animation.size();

			for (unsigned int i = 0; i < numFrames; i++)
			{
				Key frame;

				frame.SetKeyFrameTime(mesh.skeleton.joints[0].animation[i].time);

				for each (Joint joint in mesh.skeleton.joints)
				{
					Bone bone = Bone(joint.parentIndex, joint.name.c_str());
					bone.SetChildrenIndicies(joint.childIndicies);

					if (joint.animation.size() > 0)
						bone.SetGlobalMatrix(XMFLOAT4X4(joint.animation[i].transform.data));
					
					frame.AddBone(bone);
				}

				animation.AddKeyFrame(frame);
			}
	
			animation.SetDuration(animation.GetKeyFrames().back().GetKeyFrameTime());

			Interpolator interpolator;
			interpolator.SetAnimation(&animation);
			interpolators.push_back(interpolator);
		}	

		renderer.CreateTexture(mesh.texture, srv);
		renderer.CreateTexture(mesh.normalMap, nsrv);
		renderer.CreateBuffer(vBuffer, D3D11_BIND_VERTEX_BUFFER, sizeof(SIMPLE_VERTEX) * mesh.numVert, mesh.verts.data());
		renderer.CreateBuffer(iBuffer, D3D11_BIND_INDEX_BUFFER, sizeof(unsigned int) * mesh.numIndex, mesh.indicies);

	}

	// Create Shaders
	renderer.iDevice->CreateVertexShader(SimpleVertex_VS, sizeof(SimpleVertex_VS), NULL, &vShader[0]);
	renderer.iDevice->CreatePixelShader(Trivial_PS, sizeof(Trivial_PS), NULL, &pShader[0]);
	renderer.iDevice->CreateVertexShader(ShadowMapping_VS, sizeof(ShadowMapping_VS), NULL, &vShader[1]);
	renderer.iDevice->CreatePixelShader(ShadowMapping_PS, sizeof(ShadowMapping_PS), NULL, &pShader[1]);
	renderer.iDevice->CreateVertexShader(SimpleAnimation_VS, sizeof(SimpleAnimation_VS), NULL, &vShader[2]);
	renderer.iDevice->CreateVertexShader(NonAnimatedShadowMapping_VS, sizeof(NonAnimatedShadowMapping_VS), NULL, &vShader[3]);

	// Create Vert Layouts
	D3D11_INPUT_ELEMENT_DESC vLayoutDesc[] = {
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "UVW", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TANGENT", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "BONES", 0, DXGI_FORMAT_R32G32B32A32_UINT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "WEIGHTS", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	renderer.iDevice->CreateInputLayout(vLayoutDesc, 7, SimpleVertex_VS, sizeof(SimpleVertex_VS), &vertLayout[0]);

	// OBJECT
	D3D11_BUFFER_DESC cBuffDesc;
	ZeroMemory(&cBuffDesc, sizeof(cBuffDesc));

	cBuffDesc.Usage = D3D11_USAGE_DYNAMIC;
	cBuffDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cBuffDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cBuffDesc.ByteWidth = sizeof(Renderer::Object);
	cBuffDesc.MiscFlags = 0;

	renderer.iDevice->CreateBuffer(&cBuffDesc, NULL, &renderer.cBuffer[0]);


	// Skinned Object
	D3D11_BUFFER_DESC cBuffSkinDesc;
	ZeroMemory(&cBuffSkinDesc, sizeof(cBuffSkinDesc));

	cBuffSkinDesc.Usage = D3D11_USAGE_DYNAMIC;
	cBuffSkinDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cBuffSkinDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cBuffSkinDesc.ByteWidth = sizeof(Renderer::SkinnedObject);
	cBuffSkinDesc.MiscFlags = 0;

	renderer.iDevice->CreateBuffer(&cBuffSkinDesc, NULL, &renderer.cBuffer[3]);

	DEMO_APP::pDemoApp = this;
	time.Restart();
}
bool DEMO_APP::Run()
{
	time.Signal();

	renderer.ClearRenderTargetView(0.5f, 0, 0.5f, 1);
	renderer.iDeviceContext->ClearDepthStencilView(renderer.depthStencil, D3D11_CLEAR_DEPTH, 1, 0);

	// update camera
	renderer.viewMatrix.UpdateCamera((float)time.Delta());
	renderer.toShaderScene.viewMatrix = renderer.viewMatrix.GetViewMatrix();

	// update lights
	if (GetAsyncKeyState('1') && !keyPressed)
	{
		lightCameraMovementSwitch++;
		if (lightCameraMovementSwitch > 2)
			lightCameraMovementSwitch = 0;

		keyPressed = true;
	}

	// only move one light or camera at a time, from low bit - direction, point, spot
	if (lightCameraMovementSwitch == 0)
		renderer.UpdateDirectionLight((float)time.Delta(), renderer.directionalLightToShader);
	else if (lightCameraMovementSwitch == 1)
		renderer.UpdatePointLight((float)time.Delta(), renderer.pointLightToShader);
	else if (lightCameraMovementSwitch == 2)
		renderer.UpdateSpotLight((float)time.Delta(), renderer.spotLightToShader);
		

	D3D11_MAPPED_SUBRESOURCE mSubresource;
	ZeroMemory(&mSubresource, sizeof(mSubresource));

	// shadow map stuff
#if SHADOW_MAPPING
	float colorWhite[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
	renderer.iDeviceContext->ClearRenderTargetView(renderer.iSMRenderTargetView, colorWhite);
	renderer.iDeviceContext->ClearDepthStencilView(renderer.iShadowDepthStencil, D3D11_CLEAR_DEPTH, 1, 0);
	renderer.iDeviceContext->RSSetViewports(1, &renderer.smViewport);
	renderer.iDeviceContext->OMSetRenderTargets(1, &renderer.iSMRenderTargetView, renderer.iShadowDepthStencil);


	// Initialize and Create Render Chain
	CreateRenderChain(vShader[1], pShader[1],true,true);

	// build light view and projection here, might need to use camera's up vector(0, 1, 0), but i believe i can use spot light direction
	FXMVECTOR slPosition = { renderer.spotLightToShader.position.x, renderer.spotLightToShader.position.y, renderer.spotLightToShader.position.z, renderer.spotLightToShader.position.w };
	FXMVECTOR slDirection = { renderer.spotLightToShader.coneDirection.x, renderer.spotLightToShader.coneDirection.y, renderer.spotLightToShader.coneDirection.z, renderer.spotLightToShader.coneDirection.w };
	FXMVECTOR focusSlPosition = slPosition + slDirection;
	FXMVECTOR upDirection = { 0.0f, 1.0f, 0.0f, 0.0f };
	//renderer.toShaderLightViewProj.viewMatrix = XMMatrixLookAtLH(slPosition, focusSlPosition, upDirection);
	XMVECTOR tempX, tempY;
	tempX = XMVector3Normalize(XMVector3Cross(upDirection, slDirection));
	tempY = XMVector3Normalize(XMVector3Cross(slDirection, tempX));

	XMFLOAT4X4 testView(tempX.m128_f32[0], tempX.m128_f32[1], tempX.m128_f32[2], 0,
		tempY.m128_f32[0], tempY.m128_f32[1], tempY.m128_f32[2], 0,
		slDirection.m128_f32[0], slDirection.m128_f32[1], slDirection.m128_f32[2], 0,
		slPosition.m128_f32[0], slPosition.m128_f32[1], slPosition.m128_f32[2], 1);
	renderer.toShaderLightViewProj.viewMatrix = XMMatrixInverse(nullptr, XMLoadFloat4x4(&testView));

	// might need to change last argument - far clip, fsgdengine has it at 5, right now it is 8, also their fov is 0.75f
	renderer.toShaderLightViewProj.projMatrix = XMMatrixPerspectiveFovLH(acosf(renderer.spotLightToShader.outerConeRatioSL) * 2.0f, 1.0f, zNear, renderer.spotLightToShader.lightRadiusSL);

	renderer.iDeviceContext->Map(renderer.cBuffer[2], 0, D3D11_MAP_WRITE_DISCARD, 0, &mSubresource);
	memcpy(mSubresource.pData, &renderer.toShaderLightViewProj, sizeof(Renderer::Scene));
	renderer.iDeviceContext->Unmap(renderer.cBuffer[2], 0);
	renderer.iDeviceContext->VSSetConstantBuffers(1, 1, &renderer.cBuffer[2]);

	// Render Node Render
	for (unsigned int i = 0; i < contexts.size(); i++)
	{
		if (contexts[i].GetModelName() != "GroundQuad")
			contexts[i].RenderProcess();
	}

	contexts.clear();
	materials.clear();
	shapes.clear();
#endif

	// Initialize and Create Render Chain
	CreateRenderChain(vShader[2], pShader[0], true,false); // Optimized Render Chain w/ Animatings

	renderer.iDeviceContext->VSSetConstantBuffers(1, 1, &renderer.cBuffer[1]);
	renderer.iDeviceContext->OMSetRenderTargets(1, &renderer.iRenderTargetView, renderer.depthStencil);
	renderer.iDeviceContext->RSSetViewports(1, &renderer.viewport);
	renderer.iDeviceContext->PSSetSamplers(0, 1, &renderer.sampleState);
	renderer.iDeviceContext->PSSetShaderResources(2, 1, &renderer.iSMShaderResourceView);
	
	// send data to pixel shader
	renderer.iDeviceContext->Map(renderer.cBuffer[2], 0, D3D11_MAP_WRITE_DISCARD, 0, &mSubresource);
	memcpy(mSubresource.pData, &renderer.toShaderLightViewProj, sizeof(Renderer::Scene));
	renderer.iDeviceContext->Unmap(renderer.cBuffer[2], 0);
	renderer.iDeviceContext->PSSetConstantBuffers(3, 1, &renderer.cBuffer[2]);

	// lights
	renderer.iDeviceContext->Map(renderer.iDirectionalLightConstantBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mSubresource);
	memcpy(mSubresource.pData, &renderer.directionalLightToShader, sizeof(Renderer::DIRECTIONAL_LIGHT));
	renderer.iDeviceContext->Unmap(renderer.iDirectionalLightConstantBuffer, 0);
	renderer.iDeviceContext->PSSetConstantBuffers(0, 1, &renderer.iDirectionalLightConstantBuffer);
	renderer.iDeviceContext->Map(renderer.iPointLightConstantBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mSubresource);
	memcpy(mSubresource.pData, &renderer.pointLightToShader, sizeof(Renderer::POINT_LIGHT));
	renderer.iDeviceContext->Unmap(renderer.iPointLightConstantBuffer, 0);
	renderer.iDeviceContext->PSSetConstantBuffers(1, 1, &renderer.iPointLightConstantBuffer);
	renderer.iDeviceContext->Map(renderer.iSpotLightConstantBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mSubresource);
	memcpy(mSubresource.pData, &renderer.spotLightToShader, sizeof(Renderer::SPOT_LIGHT));
	renderer.iDeviceContext->Unmap(renderer.iSpotLightConstantBuffer, 0);
	renderer.iDeviceContext->PSSetConstantBuffers(2, 1, &renderer.iSpotLightConstantBuffer);

	renderer.iDeviceContext->Map(renderer.cBuffer[1], 0, D3D11_MAP_WRITE_DISCARD, 0, &mSubresource);
	memcpy(mSubresource.pData, &renderer.toShaderScene, sizeof(Renderer::Scene));
	renderer.iDeviceContext->Unmap(renderer.cBuffer[1], 0);

	// Render Node Render
	for (unsigned int i = 0; i < contexts.size(); i++)
		contexts[i].RenderProcess();

	renderer.iSwapChain->Present(0, 0);

	// want to reset for next frame
	ID3D11ShaderResourceView* ptr = 0;
	renderer.iDeviceContext->PSSetShaderResources(2, 1, &ptr);

	contexts.clear();
	materials.clear();
	shapes.clear();

	if (!GetAsyncKeyState('1') &&
		keyPressed)
	{
		keyPressed = false;
	}

	return true;
}
bool DEMO_APP::ShutDown()
{
	// need to release, directX calls new in background for you, does reference counting, anything with "I" release
	renderer.Shutdown();

	for (unsigned int i = 0; i < vBuffer.size(); i++)
		SAFE_RELEASE(vBuffer[i]);

	SAFE_RELEASE(vertLayout[0]);
	SAFE_RELEASE(vShader[0]);
	SAFE_RELEASE(pShader[0]);
	SAFE_RELEASE(vShader[1]);
	SAFE_RELEASE(pShader[1]);
	SAFE_RELEASE(vShader[2]);
	SAFE_RELEASE(vShader[3]);

	for (unsigned int i = 0; i < iBuffer.size(); i++)
		SAFE_RELEASE(iBuffer[i]);

	for (unsigned int i = 0; i < srv.size(); i++)
		SAFE_RELEASE(srv[i]);

	for (unsigned int i = 0; i < nsrv.size(); i++)
		SAFE_RELEASE(nsrv[i]);

	UnregisterClass(L"DirectXApplication", application);
	return true;
}

void DEMO_APP::Resize()
{
	renderer.Resize();
}

void DEMO_APP::CreateRenderChain(ID3D11VertexShader* VertexShader,ID3D11PixelShader* PixelShader,bool animate,bool shadow)
{
	for (unsigned int i = 0; i < fbx.meshes.size(); i++)
	{
		RenderShape shape;

		if (animate && i < interpolators.size())
		{
			if (shadow)
			{
				interpolators[i].AddTime((float)time.Delta());
				interpolators[i].Process();
			}
			shape.InitializeAnim(interpolators[i].GetPose(),&fbx.meshes[i].numIndex, interpolators[i].GetPose().size());

		}
		else
		{
			XMMATRIX world = XMMATRIX(fbx.meshes[i].matrix.data);
			shape.Initialize(&world, &fbx.meshes[i].numIndex);
		}
		

		shapes.push_back(shape);
	}

	// Create && Set Render Materials //
	for (unsigned int i = 0; i < shapes.size(); i++)
	{
		if (materials.size() > 0) // if this is not the first material created
		{
			bool foundMaterial = false;

			// check to see if the correct material already exists
			for (unsigned int mat = 0; mat < materials.size(); mat++)
			{
				if (materials[mat].GetTexName() == fbx.meshes[i].texture) // if we find the correct material, then add the current render shape to it's render set
				{
					materials[mat].AddNodeToSet(&shapes[i]);
					foundMaterial = true;
					break;
				}
			}

			// if the correct material does not exist
			if (foundMaterial == false)
			{
				RenderMaterial material;
				material.Initialize(srv[i], nsrv[i], fbx.meshes[i].texture);
				material.AddNodeToSet(&shapes[i]);

				materials.push_back(material);
			}
		}
		else // if this is the first material created
		{
			RenderMaterial material;
			material.Initialize(srv[i], nsrv[i], fbx.meshes[i].texture);
			material.AddNodeToSet(&shapes[i]);

			materials.push_back(material);
		}
	}

		// Create && Set Render Contexts //
		for (unsigned int i = 0; i < shapes.size(); i++ )
		{	
			if (contexts.size() > 0) // if this is not the first context created
			{
				bool foundContext = false;
				bool alreadyExists = false;

				// Check to see if the correct context already exists
					for (unsigned int con = 0; con < contexts.size(); con++)
					{
						for (unsigned int mat = 0; mat < materials.size(); mat++)
						{
							// if we find the correct context and the current material doesn't already exists, then add the current render material to it's render set
							if (contexts[con].GetModelName() == fbx.meshes[i].name && materials[mat].GetTexName() == fbx.meshes[i].texture)
							{
								if (contexts[con].GetSet().IsInSet(&materials[mat]) == false)
								{
									contexts[con].AddNodeToSet(&materials[mat]);
									foundContext = true;
									break;
								}
								else
									alreadyExists = true;
							}
						}
						if (foundContext || alreadyExists)
							break;
					}


				// if the correct context does not exist
				if (foundContext == false && alreadyExists == false)
				{
					RenderContext context;
					if (animate)
					{
						if (i < interpolators.size())
							context.Initialize(vBuffer[i], vertLayout[0], iBuffer[i], VertexShader, PixelShader, fbx.meshes[i].name);
						else if (shadow)
							context.Initialize(vBuffer[i], vertLayout[0], iBuffer[i], vShader[4], pShader[0], fbx.meshes[i].name);
						else
							context.Initialize(vBuffer[i], vertLayout[0], iBuffer[i], vShader[0], pShader[0], fbx.meshes[i].name);
					}
					else
						context.Initialize(vBuffer[i], vertLayout[0], iBuffer[i], VertexShader, PixelShader, fbx.meshes[i].name);

					for (unsigned int mat = 0; mat < materials.size(); mat++)
					{
						if (materials[mat].GetTexName() == fbx.meshes[i].texture)
						{
							context.AddNodeToSet(&materials[mat]);
							break;
						}
					}

					contexts.push_back(context);
				}
			}
			else // if this is the first context created
			{
				RenderContext context;
				if (animate)
				{
					if (i < interpolators.size())
						context.Initialize(vBuffer[i], vertLayout[0], iBuffer[i], VertexShader, PixelShader, fbx.meshes[i].name);
					else if (shadow)
						context.Initialize(vBuffer[i], vertLayout[0], iBuffer[i], vShader[4], pShader[0], fbx.meshes[i].name);
					else
						context.Initialize(vBuffer[i], vertLayout[0], iBuffer[i], vShader[0], pShader[0], fbx.meshes[i].name);
				}
				else
					context.Initialize(vBuffer[i], vertLayout[0], iBuffer[i], VertexShader, PixelShader, fbx.meshes[i].name);

				context.AddNodeToSet(&materials.front());

				contexts.push_back(context);
			}

		}
}