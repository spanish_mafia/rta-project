#pragma once
#include <vector>
#include <string>
#define FBXSDK_SHARED
#include <fbxsdk.h>
#include "Simple_Vertex.h"
struct KeyFrame;
struct Matrix
{
	float data[16];
};

struct Joint
{
	int parentIndex;
	FbxAMatrix globalBindInverseMatrix;
	FbxNode * linkNode;
	std::string name;
	std::vector<int> childIndicies;
	std::vector<KeyFrame> animation;
};
struct KeyFrame
{
	int frameIndex;
	FbxAMatrix globalTransform;
	Matrix transform;
	float time;
};
struct Skeleton
{
	std::vector<Joint> joints;
};
struct Mesh
{
	std::string name;
	int numVert;
	int numIndex;
	std::vector<SIMPLE_VERTEX> verts;
	int * indicies;
	Matrix matrix;
	std::string texture;
	std::string normalMap;
	Skeleton skeleton;
	unsigned int animationLength;
};
enum LightType
{
	Directional,Point,Spot
};
struct Light
{
	int type;
	float color[4];
	float position[4];
	float direction[4];
	float coneRatio;
	float innerConeRatio;
	float lightRadius;
	float outerConeRatio;
};
enum ObjectType
{
	Model, Lighting
};
class FbxLoader
{
private:
	std::vector<Mesh> LoadMesh(FbxNode* node);
	void SaveToSMPP(const std::string& file_name, std::vector<Mesh>& writeMesh);
	void LoadFromSMPP(const std::string& file_name);
	bool Load(const std::string& file_name);
	void LoadLights(std::ifstream& iFile);
	void LoadSkeletonHierarchy(FbxNode* node,Mesh& mesh, int depth, int index, int parentIndex);
	void LoadJointAndAnimation(FbxNode*node,FbxMesh* fMesh,Mesh& mesh);
	FbxAMatrix GetGeoMatrix(FbxMesh* fMesh);
	std::string namesFile;
public:
	FbxLoader();
	FbxLoader(const std::string m_namesFile);
	~FbxLoader();
	void setNamesFile(const std::string m_namesFile){ namesFile = m_namesFile; }
	void BeginLoading();
	FbxScene * scene;
	std::vector<Mesh> meshes; 
	std::vector<Light> lights;
};

