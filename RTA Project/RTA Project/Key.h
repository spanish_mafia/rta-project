#pragma once
#include "Renderer.h"
#include "Bone.h"

class Key
{
public:
	Key();
	~Key();

	void SetKeyFrameTime(float _kfTime) { keyFrameTime = _kfTime; }
	float GetKeyFrameTime() { return keyFrameTime; }

	void AddBone(Bone bone) { bones.push_back(bone); }
	std::vector<Bone>& GetBones() { return bones; }

private:
	float keyFrameTime;
	std::vector<Bone> bones;
};

