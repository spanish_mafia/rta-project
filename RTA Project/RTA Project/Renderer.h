#pragma once
#include "RenderNode.h"
#include "Camera.h"
#include "DDSTextureLoader.h"
#include "Simple_Vertex.h"

#include <d3d11.h>
#include <DirectXMath.h>
// how to link to directx 11 library, need this special path because not windows 8
#pragma comment (lib,"d3d11.lib")
using namespace DirectX;

#include <vector>
using namespace std;

class RenderSet;

class Renderer
{
public:
	Renderer() {};
	~Renderer() {};

	// used for creation
	static ID3D11Device *iDevice;
	// used to manipulate data, immediate issues commands directly to gpu, deferred waits
	static  ID3D11DeviceContext *iDeviceContext;
	// where we want to draw to, could be multiple buffers
	static  ID3D11RenderTargetView *iRenderTargetView;
	// communicates with windows and manages the drawing area, switches between buffers
	// front buffer - displayed to user, back buffer - draw target
	static  IDXGISwapChain *iSwapChain;
	// what portion of screen you want to draw to
	static  D3D11_VIEWPORT viewport;
	// main area where you are drawing to
	static  ID3D11Texture2D *iBackBuffer;
	// holds depth values for each pixel
	static ID3D11Texture2D* zBuffer;
	static ID3D11DepthStencilView* depthStencil;
	// what we use to send data from cpu to gpu
	static ID3D11Buffer* cBuffer[4];
	// camera
	static Camera viewMatrix;
	// SamplerState
	static ID3D11SamplerState* sampleState;
	// holds depth info for shadow map
	static ID3D11Texture2D *iShadowMapBuffer;
	// render target for shadow map
	static  ID3D11RenderTargetView *iSMRenderTargetView;
	// shader resource view for shadow map
	static ID3D11ShaderResourceView* iSMShaderResourceView;
	// viewport for shadow map
	static D3D11_VIEWPORT smViewport;
	// Rasterize
	static ID3D11RasterizerState* iRasterizerState;
	// depth stencil for shadow
	static ID3D11DepthStencilView* iShadowDepthStencil;
	// TEMP DEPTH BUFFER
	static ID3D11Texture2D *iSMDepthBuffer;
	struct Object
	{
		XMMATRIX worldMatrix;
		Object() {};
		Object(XMMATRIX& _world) { worldMatrix = _world; }
	};

	static Object toShaderObj;

	struct Scene
	{
		XMMATRIX viewMatrix;
		XMMATRIX projMatrix;
		Scene() {};
	};

	static Scene toShaderScene;
	static Scene toShaderLightViewProj;

	struct SkinnedObject
	{
		XMMATRIX skinnedMatrices[37];
	};

	static SkinnedObject toShaderSkinnedObj;

	// lights
	struct DIRECTIONAL_LIGHT
	{
		XMFLOAT4 direction;
		XMFLOAT4 color;
	};
	static DIRECTIONAL_LIGHT directionalLightToShader;
	static ID3D11Buffer *iDirectionalLightConstantBuffer;

	struct POINT_LIGHT
	{
		XMFLOAT4 position;
		XMFLOAT4 color;
		XMFLOAT4 lightRadiusPL;
	};
	static POINT_LIGHT pointLightToShader;
	static ID3D11Buffer *iPointLightConstantBuffer;

	struct SPOT_LIGHT
	{
		XMFLOAT4 coneDirection;
		XMFLOAT4 position;
		XMFLOAT4 color;
		float coneRatio;
		float lightRadiusSL;
		float innerConeRatioSL;
		float outerConeRatioSL;
	};
	static SPOT_LIGHT spotLightToShader;
	static ID3D11Buffer *iSpotLightConstantBuffer;

	static void Initialize(HWND hWnd);

	static void Shutdown();

	static void Render(RenderSet& set);

	static void ClearRenderTargetView(float r, float g, float b, float a);

	static void Resize();

	static void CreateTexture(string filename,vector<ID3D11ShaderResourceView*>& srv);

	static void CreateBuffer(std::vector<ID3D11Buffer*>& buff,UINT bindFlags,UINT byteWidth, const void* sysMem);

	template<class type>
	static void CreateConstantBuffer(type &bufferStructType, ID3D11Buffer **constantBuffer, ID3D11Device *device, unsigned int numElements = 1);

	static void SendObjectData(XMMATRIX& _world);

	static void SendSkinnedObjectData(std::vector<XMMATRIX>& skinMats);

	static void UpdateDirectionLight(float deltaTime, DIRECTIONAL_LIGHT &directionalLight);
	static void UpdatePointLight(float deltaTime, POINT_LIGHT &pointLight);
	static void UpdateSpotLight(float deltaTime, SPOT_LIGHT &spotLight);
};

template<class type>
static void Renderer::CreateConstantBuffer(type &bufferStructObject, ID3D11Buffer **constantBuffer, ID3D11Device *device, unsigned int numElements)
{
	D3D11_BUFFER_DESC constantBufferDesc;
	ZeroMemory(&constantBufferDesc, sizeof(constantBufferDesc));
	constantBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	constantBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	constantBufferDesc.ByteWidth = (sizeof(bufferStructObject) * numElements);
	constantBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	device->CreateBuffer(&constantBufferDesc, NULL, constantBuffer);
}


