#include "RenderMaterial.h"

RenderMaterial::RenderMaterial()
{
	numSRV = 0;
	srvs[0] = nullptr;
	srvs[1] = nullptr;
	texName = "";

	ClearSet();
}

RenderMaterial::~RenderMaterial()
{
}

void RenderMaterial::Initialize(ID3D11ShaderResourceView* diffuse, ID3D11ShaderResourceView* normal, std::string _texName)
{
	ClearSet();

	numSRV = 0;
	if (diffuse != nullptr)
	{
		srvs[0] = diffuse;
		numSRV++;
	}

	if (normal != nullptr)
	{
		srvs[1] = normal;
		numSRV++;
	}

	texName = _texName;

	func = DiffuseNormalRFunc;
}

void RenderMaterial::DiffuseNormalRFunc(RenderNode& node)
{
	RenderMaterial& material = (RenderMaterial&)node;

	Renderer::iDeviceContext->PSSetShaderResources(0, material.GetNumResources(), material.GetResources());
	Renderer::Render(material.GetSet());

}