#include "Interpolator.h"
#include "FbxLoader.h"

Interpolator::Interpolator()
{
	currentTime = 0.0f;
	animationPtr = nullptr;
}

float Interpolator::GetTime()
{
	return currentTime;
}

std::vector<XMMATRIX> Interpolator::GetPose()
{
	return pose;
}

void Interpolator::AddTime(float timeToAdd)
{
	SetTime(currentTime + timeToAdd); 
}

void Interpolator::SetTime(float _time)
{
	if (animationPtr == nullptr)
		currentTime = 0.0f;
	else
	{
		currentTime = _time;

		if (_time > animationPtr->GetDuration())
			currentTime = currentTime - animationPtr->GetDuration();
		else if (_time < 0.0f)
			currentTime = currentTime + animationPtr->GetDuration();
	}
}

void Interpolator::Process()
{
	if (animationPtr == nullptr)
	{
		pose.clear();
		return;
	}

	ProcessKeyFrames(animationPtr->GetKeyFrames());
}

void Interpolator::ProcessKeyFrames(std::vector<Key>& kFrames)
{
	float tweenTime = 0.0f;
	float frameTime = 0.0f;
	
	int previous = 0;
	int next = 0;

	// fill out variables here
	for (unsigned int i = 0; i < kFrames.size(); i++)
	{
		if (currentTime < kFrames[i].GetKeyFrameTime())
		{
			previous = i - 1;

			if (previous == -1)
				previous = kFrames.size() - 1;

			next = i;
			break;
		}
		else if (currentTime > kFrames.back().GetKeyFrameTime())
		{
			next = 0;
			previous = kFrames.size() - 1;
			break;
		}
		else if (currentTime < 0.0f)
		{
			next = kFrames.size() - 1;
			previous = 0;
			break;
		}
	}

	tweenTime = kFrames[next].GetKeyFrameTime() - kFrames[previous].GetKeyFrameTime();
	frameTime = currentTime - kFrames[previous].GetKeyFrameTime();

	float t = frameTime / tweenTime;
	
	pose.clear();

	unsigned int numBones = kFrames[0].GetBones().size();
	for (unsigned int i = 0; i < numBones; i++)
	{
		XMFLOAT4X4 boneMat;
		for (unsigned int r = 0; r < 4; r++)
		{
			for (unsigned int c = 0; c < 4; c++)
			{
				boneMat.m[r][c] = (1 - t) * kFrames[previous].GetBones()[i].GetGlobalMatrix().m[r][c] + t * kFrames[next].GetBones()[i].GetGlobalMatrix().m[r][c];
			}
		}

		pose.push_back(XMLoadFloat4x4(&boneMat));
	}
}