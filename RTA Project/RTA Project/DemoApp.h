#pragma once
#include "Defines.h"
#include "XTime.h"
#include "Renderer.h"
#include "FbxLoader.h"

#include "RenderShape.h"
#include "RenderMaterial.h"
#include "RenderContext.h"
#include "Bone.h"
#include "Key.h"
#include "Animation.h"
#include "Interpolator.h"

class DEMO_APP
{
	HINSTANCE						application;
	WNDPROC							appWndProc;
	HWND							window;

	Renderer renderer;
	XTime time = 0;
	FbxLoader fbx;
	
	std::vector<ID3D11Buffer*> vBuffer;
	ID3D11InputLayout* vertLayout[1];
	ID3D11VertexShader* vShader[4];
	ID3D11PixelShader* pShader[2];
	std::vector<ID3D11Buffer*> iBuffer;
	std::vector<ID3D11ShaderResourceView*> srv;
	std::vector<ID3D11ShaderResourceView*> nsrv;

	std::vector<RenderShape> shapes;
	std::vector<RenderMaterial> materials;
	std::vector<RenderContext> contexts;

	Animation animation;
	std::vector<Interpolator> interpolators;
	

	int lightCameraMovementSwitch = 0;
	bool keyPressed = false;

public:
	static DEMO_APP *pDemoApp;
	void Resize();
	DEMO_APP(HINSTANCE hinst, WNDPROC proc);
	bool Run();
	bool ShutDown();
	void CreateRenderChain(ID3D11VertexShader* VertexShader, ID3D11PixelShader* PixelShader, bool animate, bool shadow);
};
