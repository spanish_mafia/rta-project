#include "FbxLoader.h"
#include <DirectXMath.h>
#include <iostream>
#include <fstream>
using namespace DirectX;

FbxLoader::FbxLoader()
{
}

FbxLoader::FbxLoader(const std::string m_namesFile)
{
	namesFile = m_namesFile;
}

FbxLoader::~FbxLoader()
{
}

void FbxLoader::BeginLoading()
{
	std::ifstream iFile;
	std::string tempString;
	iFile.open(namesFile, std::ios_base::in);
	if (iFile.is_open())
	{

		while (std::getline(iFile,tempString))
		{
			int type = std::stoi(tempString);
			switch (type)
			{
			case Model:
			{
				std::string fileName, trash;
				std::getline(iFile, fileName);
				Load(fileName);
				Matrix tempMatrix;
				for (int i = 0; i < 16; i++)
				{
					std::string tempFloat;
					iFile >> tempFloat;
					tempMatrix.data[i] = std::stof(tempFloat);
				}
				meshes[meshes.size() - 1].matrix = tempMatrix;
				std::getline(iFile, trash);
				std::string textureName;
				std::getline(iFile,textureName);
				meshes[meshes.size() - 1].texture = textureName;
				std::string normalMapName;
				std::getline(iFile, normalMapName);
				meshes[meshes.size() - 1].normalMap = normalMapName;
				meshes[meshes.size() - 1].name = fileName;
				break;
			}
			case Lighting:
			{
				LoadLights(iFile);
				break;
			}
			default:
				break;
			}
		}
		iFile.close();
	}
}

void FbxLoader::LoadLights(std::ifstream& iFile)
{
	std::string lightType, color, position, direction;
	Light tempLight;
	std::getline(iFile, lightType);
	tempLight.type = std::stoi(lightType);
	for (int i = 0; i < 4; i++)
	{
		iFile >> color;
		tempLight.color[i] = std::stof(color);
	}
	std::getline(iFile,color);
	switch (tempLight.type)
	{
	case Point:
	{
		for (int i = 0; i < 4; i++)
		{
			iFile >> position;
			tempLight.position[i] = std::stof(position);
		}
		std::getline(iFile, position);
		break;

	}
	case Directional:
	{
		for (int i = 0; i < 4; i++)
		{
			iFile >> direction;
			tempLight.direction[i] = std::stof(direction);
		}
		std::getline(iFile, position);
		break;
	}
	case Spot:
	{
		for (int i = 0; i < 4; i++)
		{
			iFile >> position;
			tempLight.position[i] = std::stof(position);
		}
		std::getline(iFile, position);
		for (int i = 0; i < 4; i++)
		{
			iFile >> direction;
			tempLight.direction[i] = std::stof(direction);
		}
		std::getline(iFile, position);
		std::string tempFloat;
		std::getline(iFile, tempFloat);
		tempLight.coneRatio = std::stof(tempFloat);
		std::getline(iFile, tempFloat);
		tempLight.innerConeRatio = std::stof(tempFloat);
		std::getline(iFile, tempFloat);
		tempLight.lightRadius = std::stof(tempFloat);
		std::getline(iFile, tempFloat);
		tempLight.outerConeRatio = std::stof(tempFloat);
		break;
	}
	default:
		break;
	}
	lights.push_back(tempLight);
}

bool FbxLoader::Load(const std::string& fileName)
{
	std::string tempName = fileName;
	tempName.append(".smpp");
	std::ifstream iFile(tempName);
	if (!iFile.good())
	{
		iFile.close();
		tempName = fileName;
		tempName.append(".fbx");
		FbxManager* fbxManager = FbxManager::Create();
		FbxIOSettings * ioSettings = FbxIOSettings::Create(fbxManager, IOSROOT);
		FbxImporter * importer = FbxImporter::Create(fbxManager, "");
		FbxScene * fbxScene = FbxScene::Create(fbxManager, "RTAScene");
		scene = fbxScene;
		importer->Initialize(tempName.c_str(), -1, ioSettings);
		importer->Import(fbxScene);
		importer->Destroy();
		FbxGeometryConverter  geoConverter(fbxManager);
		geoConverter.Triangulate(fbxScene, true);
		geoConverter.SplitMeshesPerMaterial(fbxScene, true);
		FbxNode *rootNode = fbxScene->GetRootNode();
		std::vector<Mesh> tempMesh = LoadMesh(rootNode);
		tempName = fileName;
		tempName.append(".smpp");
		//SaveToSMPP(tempName,tempMesh);
	}
	else
	{
		iFile.close();
		LoadFromSMPP(tempName);
	}
	return true;
}

void FbxLoader::SaveToSMPP(const std::string& file_name,std::vector<Mesh>& writeMesh)
{
	std::fstream oFile;
	oFile.open(file_name.c_str(), std::ios_base::out | std::ios_base::binary);
	if (oFile.is_open())
	{
		unsigned int size = (unsigned int)writeMesh.size();
		oFile.write((char*)&size, sizeof(size));
		for (unsigned int i = 0; i < size; i++)
		{
			oFile.write((char*)&writeMesh[i].numVert, sizeof(int));
			oFile.write((char*)&writeMesh[i].numIndex, sizeof(int));
			for (int j = 0; j < writeMesh[i].numVert; j++)
			{
				oFile.write((char*)&writeMesh[i].verts[j].color, sizeof(float) * 4);
				oFile.write((char*)&writeMesh[i].verts[j].controlPointIndex, sizeof(int));
				oFile.write((char*)&writeMesh[i].verts[j].index, sizeof(int) * 4);
				oFile.write((char*)&writeMesh[i].verts[j].normal, sizeof(float) * 4);
				int numPairs = writeMesh[i].verts[j].pairs.size();
				oFile.write((char*)&numPairs, sizeof(numPairs));
				for (int k = 0; k < numPairs; k++)
				{
					oFile.write((char*)&writeMesh[i].verts[j].pairs[k], sizeof(BlendingPair));
				}
				oFile.write((char*)&writeMesh[i].verts[j].position, sizeof(float)*4);
				oFile.write((char*)&writeMesh[i].verts[j].tangent, sizeof(float)*4);
				oFile.write((char*)&writeMesh[i].verts[j].uvw, sizeof(float)*3);
				oFile.write((char*)&writeMesh[i].verts[j].weight, sizeof(float)*4);
			}
			for (int j = 0; j < writeMesh[i].numIndex; j++)
			{
				oFile.write((char*)&writeMesh[i].indicies[j], sizeof(int));
			}
			oFile.write((char*)&writeMesh[i].animationLength, sizeof(unsigned int));
			int numJoints = writeMesh[i].skeleton.joints.size();
			oFile.write((char*)&numJoints, sizeof(numJoints));
			for (unsigned int j = 0; j < writeMesh[i].skeleton.joints.size(); j++)
			{
				int nameLen = sizeof(nameLen);
				oFile.write((char*)nameLen, sizeof(nameLen));
				oFile.write((char*)&writeMesh[i].skeleton.joints[j].name, nameLen);
				oFile.write((char*)&writeMesh[i].skeleton.joints[j].globalBindInverseMatrix, sizeof(Matrix));
				oFile.write((char*)&writeMesh[i].skeleton.joints[j].parentIndex, sizeof(int));
				int numKeyFrame = writeMesh[i].skeleton.joints[j].animation.size();
				oFile.write((char*)&numKeyFrame, sizeof(numKeyFrame));
				for (unsigned int k = 0; k < writeMesh[i].skeleton.joints[j].animation.size(); k++)
				{
					oFile.write((char*)&writeMesh[i].skeleton.joints[j].animation[k], sizeof(KeyFrame));
				}
				int numChildIndicies = writeMesh[i].skeleton.joints[j].childIndicies.size();
				oFile.write((char*)&numChildIndicies, sizeof(numChildIndicies));
				for (unsigned int k = 0; k < writeMesh[i].skeleton.joints[j].childIndicies.size(); k++)
				{
					oFile.write((char*)&writeMesh[i].skeleton.joints[j].childIndicies[k], sizeof(writeMesh[i].skeleton.joints[j].childIndicies[k]));
				}
			}
		}
		oFile.close();
	}
}
void FbxLoader::LoadFromSMPP(const std::string& file_name)
{
	std::ifstream iFile;
	iFile.open(file_name.c_str(), std::ios_base::in | std::ios_base::binary);
	if (iFile.is_open())
	{
		unsigned int size = 0;
		iFile.read((char*)&size, sizeof(unsigned int));
		for (unsigned int i = 0; i < size; i++)
		{
			Mesh mesh;
			iFile.read((char*)&mesh.numVert, sizeof(int));
			iFile.read((char*)&mesh.numIndex, sizeof(int));
			mesh.indicies = new int[mesh.numIndex];
			for (int j = 0; j < mesh.numVert; j++)
			{
				SIMPLE_VERTEX temp;
				//oFile.write((char*)&writeMesh[i].verts[j].color, sizeof(float) * 4);
				//oFile.write((char*)&writeMesh[i].verts[j].controlPointIndex, sizeof(int));
				//oFile.write((char*)&writeMesh[i].verts[j].index, sizeof(int) * 4);
				//oFile.write((char*)&writeMesh[i].verts[j].normal, sizeof(float) * 4);
				//int numPairs = writeMesh[i].verts[j].pairs.size();
				//oFile.write((char*)&numPairs, sizeof(numPairs));
				//for (int k = 0; k < numPairs; k++)
				//{
				//	oFile.write((char*)&writeMesh[i].verts[j].pairs[k], sizeof(BlendingPair));
				//}
				//oFile.write((char*)&writeMesh[i].verts[j].position, sizeof(float) * 4);
				//oFile.write((char*)&writeMesh[i].verts[j].tangent, sizeof(float) * 4);
				//oFile.write((char*)&writeMesh[i].verts[j].uvw, sizeof(float) * 3);
				//oFile.write((char*)&writeMesh[i].verts[j].weight, sizeof(float) * 4);
				mesh.verts.push_back(temp);
			}
			for (int j = 0; j < mesh.numIndex; j++)
			{
				iFile.read((char*)&mesh.indicies[j], sizeof(int));
			}
			iFile.read((char*)&mesh.animationLength, sizeof(unsigned int));
			int numJoints;
			iFile.read((char*)&numJoints, sizeof(numJoints));
			for (int j = 0; j < numJoints; j++)
			{
				Joint joint;
				int nameLen;
				iFile.read((char*)&nameLen, sizeof(nameLen));
				iFile.read((char*)&joint.name, nameLen);
				iFile.read((char*)&joint.globalBindInverseMatrix, sizeof(Matrix));
				iFile.read((char*)&joint.parentIndex, sizeof(int));
				int numKeyFrame;
				iFile.read((char*)&numKeyFrame, sizeof(int));
				for (int k = 0; k < numKeyFrame; k++)
				{
					KeyFrame keyFrame;
					iFile.read((char*)&keyFrame, sizeof(KeyFrame));
					joint.animation.push_back(keyFrame);
				}
				int numChildIndecies;
				iFile.read((char*)&numChildIndecies, sizeof(int));
				for (int k = 0; k < numChildIndecies; k++)
				{
					int childIndex;
					iFile.read((char*)&childIndex, sizeof(int));
					joint.childIndicies.push_back(childIndex);
				}
			}
			meshes.push_back(mesh);
		}
		iFile.close();
	}
}
void FbxLoader::LoadSkeletonHierarchy(FbxNode* node,Mesh& mesh, int depth = 0, int index = 0, int parentIndex = -1)
{
	if (node->GetNodeAttribute() && node->GetNodeAttribute()->GetAttributeType() && node->GetNodeAttribute()->GetAttributeType() == FbxNodeAttribute::eSkeleton)
	{
		Joint joint;
		joint.parentIndex = parentIndex;
		joint.name = node->GetName();
		mesh.skeleton.joints.push_back(joint);
		if (joint.parentIndex > 0)
		{
			mesh.skeleton.joints[parentIndex].childIndicies.push_back(index);
		}
	}
	for (int i = 0; i < node->GetChildCount(); i++)
	{
		LoadSkeletonHierarchy(node->GetChild(i), mesh, depth + 1, mesh.skeleton.joints.size(), index);
	}
}

void FbxLoader::LoadJointAndAnimation(FbxNode * node,FbxMesh* fMesh, Mesh& mesh)
{
	unsigned int numDeformer = fMesh->GetDeformerCount();
	FbxAMatrix geoMatrix = GetGeoMatrix(fMesh);
	for (unsigned int i = 0; i < numDeformer; i++)
	{
		FbxSkin* fSkin = (FbxSkin*)fMesh->GetDeformer(i, FbxDeformer::eSkin);
		if (fSkin != nullptr)
		{
			for (int j = 0; j < fSkin->GetClusterCount(); j++)
			{
				FbxCluster * fCluster = fSkin->GetCluster(j);
				int jointIndex = -1;
				for (unsigned int k = 0; k < mesh.skeleton.joints.size(); k++)
				{
					if (fCluster->GetLink()->GetName() == mesh.skeleton.joints[k].name)
					{
						jointIndex = k;
						break;
					}
				}
				FbxAMatrix tMatrix, tLMatrix, iGBPMatrix;
				fCluster->GetTransformMatrix(tMatrix);
				fCluster->GetTransformLinkMatrix(tLMatrix);
				iGBPMatrix = tLMatrix.Inverse() * tMatrix * geoMatrix;
				mesh.skeleton.joints[jointIndex].globalBindInverseMatrix = iGBPMatrix;
				mesh.skeleton.joints[jointIndex].linkNode = fCluster->GetLink();
				for (int k = 0; k < fCluster->GetControlPointIndicesCount(); k++)
				{
					BlendingPair pair;
					pair.index = jointIndex;
					pair.weight = (float)fCluster->GetControlPointWeights()[k];
					int cpIndex = fCluster->GetControlPointIndices()[k];
					for (unsigned int vertIndex = 0; vertIndex < mesh.verts.size(); vertIndex++)
					{
						if (mesh.verts[vertIndex].controlPointIndex == cpIndex)
						{
							mesh.verts[vertIndex].pairs.push_back(pair);
						}
					}
				}
				FbxAnimStack* currAnimStack = scene->GetSrcObject<FbxAnimStack>();
				FbxString animStackName = currAnimStack->GetName();	
				FbxTakeInfo* takeInfo = scene->GetTakeInfo(animStackName);
				FbxTime start = takeInfo->mLocalTimeSpan.GetStart();
				FbxTime end = takeInfo->mLocalTimeSpan.GetStop();
				mesh.animationLength = (unsigned int)(end.GetFrameCount(FbxTime::eFrames24) - start.GetFrameCount(FbxTime::eFrames24) + 1);
				KeyFrame currAnim;
				for (FbxLongLong k = start.GetFrameCount(FbxTime::eFrames24); k <= end.GetFrameCount(FbxTime::eFrames24); ++k)
				{
					FbxTime currTime;
					currTime.SetFrame(k, FbxTime::eFrames24);
					currAnim.frameIndex = (int)k;
					FbxAMatrix currentTransformOffset = node->EvaluateGlobalTransform(currTime);
					currAnim.globalTransform = currentTransformOffset.Inverse() * fCluster->GetLink()->EvaluateGlobalTransform(currTime);
					currAnim.transform = { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 };
					int currentArray = 0;
					for (int l = 0; l < 16; l+=4)
					{
						currAnim.transform.data[l] = (float)currAnim.globalTransform.mData[currentArray].mData[0];
						currAnim.transform.data[l + 1] = (float)currAnim.globalTransform.mData[currentArray].mData[1];
						currAnim.transform.data[l + 2] = (float)currAnim.globalTransform.mData[currentArray].mData[2];
						currAnim.transform.data[l + 3] = (float)currAnim.globalTransform.mData[currentArray].mData[3];
						currentArray++;
					}
					currAnim.time = (float)currTime.GetSecondDouble();
					mesh.skeleton.joints[jointIndex].animation.push_back(currAnim);
				}

			}
		}
	}

}
FbxAMatrix FbxLoader::GetGeoMatrix(FbxMesh * fMesh)
{
	FbxVector4 translation = fMesh->GetNode()->GetGeometricTranslation(FbxNode::eSourcePivot);
	FbxVector4 rotation = fMesh->GetNode()->GetGeometricRotation(FbxNode::eSourcePivot);
	FbxVector4 scaling = fMesh->GetNode()->GetGeometricScaling(FbxNode::eSourcePivot);
	return FbxAMatrix(translation, rotation, scaling);
}
std::vector<Mesh> FbxLoader::LoadMesh(FbxNode * node)
{
	int numChildren = node->GetChildCount();
	int vertex_id;
	std::vector<Mesh> tempMeshes;
	for (int i = 0; i < numChildren; i++)
	{
		std::vector<int> cpIndecies;
		FbxNode* cNode = node->GetChild(i);
		FbxMesh * fMesh = cNode->GetMesh();
		Mesh mesh;
		if (fMesh != nullptr)
		{
			SIMPLE_VERTEX tempVert;
			int polygon_count = fMesh->GetPolygonCount();
			FbxVector4* control_points = fMesh->GetControlPoints();

			int control_point_count = fMesh->GetControlPointsCount();

			vertex_id = 0;

			LoadSkeletonHierarchy(node, mesh);
			for (int polygon = 0; polygon < polygon_count; ++polygon)
			{
				int polygon_vertex_count = 3;
				for (int polygon_vertex = 0; polygon_vertex < polygon_vertex_count; ++polygon_vertex)
				{
					
					int control_point_index = fMesh->GetPolygonVertex(polygon, polygon_vertex);
					FbxAMatrix mat_geometry = GetGeoMatrix(fMesh);

					FbxVector4 position;
					position = control_points[control_point_index].mData;
					position = mat_geometry.MultT(position);
					tempVert.controlPointIndex = control_point_index;
					tempVert.position[0] = (float)position.mData[0];
					tempVert.position[1] = (float)position.mData[1];
					tempVert.position[2] = (float)position.mData[2];
					// Get texture coords
					int uv_element_count = fMesh->GetElementUVCount();
					for (int uv_element = 0; uv_element < uv_element_count; ++uv_element) 
					{
						FbxGeometryElementUV* geometry_element_uv = fMesh->GetElementUV(uv_element);
						FbxLayerElement::EMappingMode mapping_mode = geometry_element_uv->GetMappingMode();
						FbxLayerElement::EReferenceMode reference_mode = geometry_element_uv->GetReferenceMode();
						int direct_index = -1;
						switch (mapping_mode)
						{
						case FbxGeometryElement::eByControlPoint:
						{
							if (reference_mode == FbxGeometryElement::eDirect)
							{
								direct_index = control_point_index;
							}
							else if (reference_mode == FbxGeometryElement::eIndexToDirect)
							{
								direct_index = geometry_element_uv->GetIndexArray().GetAt(control_point_index);
							}
							break;
						}
						case FbxGeometryElement::eByPolygonVertex:
						{
							if (reference_mode == FbxGeometryElement::eDirect ||
								reference_mode == FbxGeometryElement::eIndexToDirect)
							{
								direct_index = fMesh->GetTextureUVIndex(polygon, polygon_vertex);
							}
							break;
						}
						default:
							break;
						}
						if (direct_index != -1)
						{
							FbxVector2 uv = geometry_element_uv->GetDirectArray().GetAt(direct_index);
							tempVert.uvw[0] = (float)uv.mData[0];
							tempVert.uvw[1] = (float)uv.mData[1];
						}
					}

					// Get normal
					int normal_element_count = fMesh->GetElementNormalCount();
					for (int normal_element = 0; normal_element < normal_element_count; ++normal_element)
					{
						FbxGeometryElementNormal* geometry_element_normal = fMesh->GetElementNormal(normal_element);

						FbxLayerElement::EMappingMode mapping_mode = geometry_element_normal->GetMappingMode();
						FbxLayerElement::EReferenceMode reference_mode = geometry_element_normal->GetReferenceMode();

						int direct_index = -1;

						if (mapping_mode == FbxGeometryElement::eByPolygonVertex)
						{
							if (reference_mode == FbxGeometryElement::eDirect)
							{
								direct_index = vertex_id;
							}
							else if (reference_mode == FbxGeometryElement::eIndexToDirect)
							{
								direct_index = geometry_element_normal->GetIndexArray().GetAt(vertex_id);
							}
						}

						if (direct_index != -1)
						{
							FbxVector4 norm = geometry_element_normal->GetDirectArray().GetAt(direct_index);

							tempVert.normal[0] = (float)norm.mData[0];
							tempVert.normal[1]= (float)norm.mData[1];
							tempVert.normal[2]= (float)norm.mData[2];
						}
					}
					int tangent_element;
					int tangent_element_count = fMesh->GetElementTangentCount();
					for (tangent_element = 0; tangent_element < tangent_element_count; ++tangent_element)
					{
						FbxGeometryElementTangent* geometry_element_tangent = fMesh->GetElementTangent(tangent_element);

						FbxLayerElement::EMappingMode mapping_mode = geometry_element_tangent->GetMappingMode();
						FbxLayerElement::EReferenceMode reference_mode = geometry_element_tangent->GetReferenceMode();

						int direct_index = -1;

						if (mapping_mode == FbxGeometryElement::eByPolygonVertex)
						{
							if (reference_mode == FbxGeometryElement::eDirect)
							{
								direct_index = vertex_id;
							}
							else if (reference_mode == FbxGeometryElement::eIndexToDirect)
							{
								direct_index = geometry_element_tangent->GetIndexArray().GetAt(vertex_id);
							}
						}

						if (direct_index != -1)
						{
							FbxVector4 tan = geometry_element_tangent->GetDirectArray().GetAt(direct_index);

							tempVert.tangent[0] = (float)tan.mData[0];
							tempVert.tangent[1] = (float)tan.mData[1];
							tempVert.tangent[2] = (float)tan.mData[2];
						}
					}

					std::vector< SIMPLE_VERTEX >& unique_vertices = mesh.verts;

					int size = (int)unique_vertices.size();
					int i;
					for (i = 0; i < size; ++i)
					{
						if (tempVert.normal[0] == unique_vertices[i].normal[0]&&
							tempVert.normal[1] == unique_vertices[i].normal[1]&&
							tempVert.normal[2] == unique_vertices[i].normal[2]&&
							tempVert.position[0] == unique_vertices[i].position[0] &&
							tempVert.position[1] == unique_vertices[i].position[1] &&
							tempVert.position[2] == unique_vertices[i].position[2] &&
							tempVert.tangent[0] == unique_vertices[i].tangent[0] &&
							tempVert.tangent[1] == unique_vertices[i].tangent[1] &&
							tempVert.tangent[2] == unique_vertices[i].tangent[2] &&
							tempVert.uvw[0] == unique_vertices[i].uvw[0] &&
							tempVert.uvw[1] == unique_vertices[i].uvw[1])
						{
							break;
						}
					}

					if (i == size)
					{
						unique_vertices.push_back(tempVert);
					}

					cpIndecies.push_back(i);

					++vertex_id;
				}
			}
			LoadJointAndAnimation(cNode,fMesh, mesh);
			for (unsigned int j = 0; j < mesh.verts.size(); j++)
			{
				unsigned int currIndex = 0;
				for (currIndex = 0; currIndex < mesh.verts[j].pairs.size(); currIndex++)
				{
					mesh.verts[j].index[currIndex] = mesh.verts[j].pairs[currIndex].index;
					mesh.verts[j].weight[currIndex] = mesh.verts[j].pairs[currIndex].weight;
				}
				for (; currIndex < 4; currIndex++)
				{
					mesh.verts[j].index[currIndex] = 0;
					mesh.verts[j].weight[currIndex] = 0;
				}
			
			}
			mesh.numVert = mesh.verts.size();
			mesh.numIndex = cpIndecies.size();
			mesh.indicies = new int[cpIndecies.size()];
			for (unsigned int j = 0; j < cpIndecies.size(); j++)
			{
				mesh.indicies[j] = cpIndecies[j];
			}
			meshes.push_back(mesh);
			tempMeshes.push_back(mesh);
	}
		LoadMesh(cNode);
	}
	return tempMeshes;

}
