#pragma once
#include "RenderNode.h"
class RenderShape :	public RenderNode
{
protected:
	XMMATRIX worldMatrix;
	std::vector<XMMATRIX> pose;
	int numIndices;

public:
	RenderShape();
	~RenderShape() {}

	void Initialize(XMMATRIX* MatrixPtr, int* _numIndex);
	void InitializeAnim(std::vector<XMMATRIX>& MatrixArray, int* _numIndex, int arraySize);

	XMMATRIX GetWorldMatrix() { return worldMatrix; }
	std::vector<XMMATRIX> GetPose() { return pose; }
	int GetNumIndicies() { return numIndices; }

	static void IndexedMeshRFunc(RenderNode& node);
	static void IndexedAnimMeshRFunc(RenderNode& node);
};

