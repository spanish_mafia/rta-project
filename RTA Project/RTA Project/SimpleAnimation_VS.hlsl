#pragma pack_matrix(row_major)
// #pragma above is only for hlsl, for matrix multiplication
struct INPUT_VERTEX
{
	float4 coordinate : POSITION;
	float3 uv : UVW;
	float4 normal : NORMAL;
	float4 color : COLOR;
	float4 tangent : TANGENT;
	uint4 bones : BONES;
	float4 weights : WEIGHTS;
};

struct OUTPUT_VERTEX
{
	float4 projectedCoordinate : SV_POSITION;
	float3 uvw : UVW;
	float4 normal : NORMAL;
	float4 color : COLOR;
	float4 surfacePosition : SURFACE_POSITION;
	float4 tan : TANGENT;
	float4 bit : BITANGENT;
};

cbuffer SkinnedObject : register(b0)
{
	float4x4 skinnedMatrices[37];
}

cbuffer Scene : register(b1)
{
	float4x4 viewMatrix;
	float4x4 projectionMatrix;
}


OUTPUT_VERTEX main(INPUT_VERTEX input)
{
	OUTPUT_VERTEX output = (OUTPUT_VERTEX)0;

	float4 localVertex = float4(input.coordinate.xyz, 1.0f);

	float4x4 mat = skinnedMatrices[input.bones.x] * input.weights.x;
	mat += skinnedMatrices[input.bones.y] * input.weights.y;
	mat += skinnedMatrices[input.bones.z] * input.weights.z;
	mat += skinnedMatrices[input.bones.w] * input.weights.w;

	output.surfacePosition = mul(localVertex, mat);

	output.normal = mul(float4 (input.normal.xyz, 0.0), mat);
	output.tan = mul(float4(input.tangent.xyz, 0.0f),mat);
	output.bit = float4(cross(output.normal, output.tan), 0.0f);

	output.uvw = input.uv;

	output.projectedCoordinate = mul(output.surfacePosition, viewMatrix);
	output.projectedCoordinate = mul(output.projectedCoordinate, projectionMatrix);	

	output.color = input.color;

	return output;
}